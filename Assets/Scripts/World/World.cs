﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using UnityEngine.SceneManagement;
using UnityEngine;

public class World : IXmlSerializable
{
    private string currentScene; //sceneName of where ship is
    private string additionalPlaceInfo; //Some scenes has multiple places

    private int day;
    private int money;
    private List<Unit> heroes;
    private List<Mission> currentMissions;
    private List<Mission> completedMissions;
    List<Item> allItems;


    List<Item> worldMerchantItems;

    /*
     * Missions done
     * missions pending
     * Special events
     * Resources
     * World Position
     * 
     */

    public World()
    {

    }

    public void setWorldForNewGame()
    {
        day = 1;
        money = 2000;

        heroes = new List<Unit>
        {
            new Unit("Eiger", "Barbarian", 100, 100, 4, 0, 1, 1, new List<Skill>()),
            new Unit("Glory", "Barbarian", 120, 120, 4, 0, 1, 1, new List<Skill>()),
            new Unit("Blitz", "Archer", 80, 80, 5, 0, 1, 1, new List<Skill>()),
            new Unit("Dietrich", "Druid", 65, 65, 4, 0, 1, 1, new List<Skill>())
        };

        currentMissions = new List<Mission>();
        completedMissions = new List<Mission>();
        allItems = new List<Item>();
        worldMerchantItems = new List<Item>();

        allItems.Add(new OneHandedAxe());
        allItems.Add(new LeatherCap("Glory"));

        // TODO Proper Units, missions etc
    }

    public string getCurrentScene()
    {
        return currentScene;
    }

    public void setHeroes(List<Unit> heroes)
    {
        this.heroes = heroes;
    }

    public List<Unit> getHeroes()
    {
        return heroes;
    }

    public int getMoney()
    {
        return money;
    }

    public void setMoney(int money)
    {
        this.money = money;
    }

    public List<Mission> getCompletedMissions()
    {
        return completedMissions;
    }

    public List<Mission> getCurrentMissions()
    {
        return currentMissions;
    }

    public void addNewMission(Mission newMission)
    {
        currentMissions.Add(newMission);
    }

    public List<Item> getItems()
    {
        return allItems;
    }

    public List<Item> getWorldMerchantItems()
    {
        return worldMerchantItems;
    }

    public void completeMission(Mission assignedMission)
    {
        Debug.Log("MISSION IS COMPLETED");
        currentMissions.Remove(assignedMission);
        completedMissions.Add(assignedMission);
    }

    public void WriteXml(XmlWriter writer)
    {
        writer.WriteStartElement("WorldInfo");
        writer.WriteAttributeString("sceneName", SceneManager.GetActiveScene().name);
        writer.WriteAttributeString("money", money.ToString());
        writer.WriteAttributeString("day", day.ToString());
        writer.WriteEndElement();

        if (heroes.Count > 0)
        {
            writer.WriteStartElement("Units");
            for (int i = 0; i < heroes.Count; i++)
            {
                writer.WriteStartElement("Unit");
                heroes[i].WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        if (currentMissions.Count > 0)
        {
            writer.WriteStartElement("CurrentMissions");
            for (int i = 0; i < currentMissions.Count; i++)
            {
                writer.WriteStartElement("CurrentMission");
                currentMissions[i].WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        if (completedMissions.Count > 0)
        {
            writer.WriteStartElement("CompletedMissions");
            for (int i = 0; i < completedMissions.Count; i++)
            {
                writer.WriteStartElement("CompletedMission");
                completedMissions[i].WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        if (allItems.Count > 0)
        {
            writer.WriteStartElement("Items");
            for (int i = 0; i < allItems.Count; i++)
            {
                writer.WriteStartElement("Item");
                writer.WriteAttributeString("itemName", allItems[i].name);
                writer.WriteAttributeString("heroOwner", allItems[i].heroOwner);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
    }

    public XmlSchema GetSchema()
    {
        return null;
    }

    public void ReadXml(XmlReader reader)
    {
        heroes = new List<Unit>();
        currentMissions = new List<Mission>();
        completedMissions = new List<Mission>();
        allItems = new List<Item>();
        worldMerchantItems = new List<Item>();

        while (reader.Read())
        {
            switch (reader.Name)
            {
                case "WorldInfo":
                    reader.MoveToAttribute("sceneName");
                    currentScene = reader.ReadContentAsString();
                    reader.MoveToAttribute("money");
                    money = reader.ReadContentAsInt();
                    reader.MoveToAttribute("day");
                    day = reader.ReadContentAsInt();
                    break;
                case "Units":
                    ReadXml_Units(reader);
                    break;
                case "CurrentMissions":
                    ReadXml_Missions(reader, true);
                    break;
                case "CompletedMissions":
                    ReadXml_Missions(reader, false);
                    break;
                case "Items":
                    ReadXml_Items(reader);
                    break;
            }
        }
    }

    private void ReadXml_Units(XmlReader reader)
    {
        while (reader.Read())
        {
            if (reader.Name != "Unit")
            {
                return; // Out of units to load 
            }
            reader.MoveToAttribute("name");
            string name = reader.ReadContentAsString();
            reader.MoveToAttribute("className");
            string className = reader.ReadContentAsString();
            reader.MoveToAttribute("maxHealth");
            int maxHealth = reader.ReadContentAsInt();
            reader.MoveToAttribute("currentHealth");
            int currentHealth = reader.ReadContentAsInt();
            reader.MoveToAttribute("maxMovement");
            int maxMovement = reader.ReadContentAsInt();
            reader.MoveToAttribute("totalExperience");
            int totalExperience = reader.ReadContentAsInt();
            reader.MoveToAttribute("level");
            int level = reader.ReadContentAsInt();
            reader.MoveToAttribute("skillPoints");
            int skillPoints = reader.ReadContentAsInt();

            List<Skill> skills = new List<Skill>();
            for (int i = 0; i < 99; i++)
            {
                if (reader.MoveToAttribute("skill" + i))
                {
                    skills.Add(createSkillForUnit(reader.ReadContentAsString()));
                }
                else
                {
                    break;
                }
            }

            Unit unit = new Unit(name, className, maxHealth, currentHealth, maxMovement, totalExperience, level, skillPoints, skills);
            heroes.Add(unit);
        }
    }

    private void ReadXml_Missions(XmlReader reader, bool isCurrentMission)
    {
        while (reader.Read())
        {
            string nodeName;
            if (isCurrentMission)
            {
                nodeName = "CurrentMission";
            }
            else
            {
                nodeName = "CompletedMission";
            }
            if (reader.Name != nodeName)
            {
                return; // Out of missions to load 
            }

            reader.MoveToAttribute("missionID");
            string missionID = reader.ReadContentAsString();
            reader.MoveToAttribute("missionName");
            string missionName = reader.ReadContentAsString();
            reader.MoveToAttribute("missionDescription");
            string missionDescription = reader.ReadContentAsString();
            reader.MoveToAttribute("currentState");
            string currentState = reader.ReadContentAsString();
            reader.MoveToAttribute("nextEventLocation");
            string nextEventLocation = reader.ReadContentAsString();
            reader.MoveToAttribute("scenesIndex");
            int scenesIndex = reader.ReadContentAsInt();

            List<string> missionScenes = new List<string>();
            for (int i = 0; i < 99; i++)
            {
                if (reader.MoveToAttribute("MissionScene" + i))
                {
                    missionScenes.Add(reader.ReadContentAsString());
                }
                else
                {
                    break;
                }
            }
            List<string> dialogIDs = new List<string>();
            for (int i = 0; i < 99; i++)
            {
                if (reader.MoveToAttribute("dialogId" + i))
                {
                    dialogIDs.Add(reader.ReadContentAsString());
                }
                else
                {
                    break;
                }
            }

            if (isCurrentMission)
            {
                currentMissions.Add(new Mission(missionID, missionName, missionDescription, nextEventLocation, missionScenes.ToArray(), dialogIDs.ToArray(), currentState, scenesIndex));
            }
            else
            {
                completedMissions.Add(new Mission(missionID, missionName, missionDescription, nextEventLocation, missionScenes.ToArray(), dialogIDs.ToArray(), currentState, scenesIndex));
            }

        }
    }

    private void ReadXml_Items(XmlReader reader)
    {
        while (reader.Read())
        {
            if (reader.Name != "Item")
            {
                return; // Out of items to load 
            }
            reader.MoveToAttribute("itemName");
            string itemName = reader.ReadContentAsString();
            reader.MoveToAttribute("heroOwner");
            string heroOwner = reader.ReadContentAsString();

            Item item = null;
            switch (itemName)                           //TODO try to come up with more abstract solution
            {
                case "Axe":
                    item = new OneHandedAxe(heroOwner);
                    break;
                case "Leather Cap":
                    item = new LeatherCap(heroOwner);
                    break;
            }

            if (item != null)
            {
                allItems.Add(item);
            }
        }
    }




    private Skill createSkillForUnit(string skillName)
    {
        if (skillName.Equals("Force Push"))
        {
            return new Push();
        }

        return null;
    }

}
