﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    public AudioClip grunt;

    protected GameObject BattleGroundObject;
    protected Canvas InventoryCanvas;
    protected bool showMoves;
    protected bool turningToTarget;
    protected bool attack;
    public bool useSkill;

    public Vector3 coordinates;
    public bool enteringPosition;
    public bool moving;
    public bool isActionUsed;
    public bool isHealthUpdated;
    public bool isUpdatingHealth;
    public List<Weapon> weapons;
    public List<HealingItem> healingItems;
    public Item currentItem;
    public Skill currentSkill;
    public string facingDirection;
    public List<Vector3> positions;
    public int type;
    public List<Vector3> additionalPositions;
    private GameObject currentVisualEffect;
    protected Animator anim;
    protected AudioSource audioSource;

    protected List<GameObject> movementHighlights;
    protected List<GameObject> weaponHighlights;
    protected List<GameObject> weaponAreaEffectHighlights;
    protected List<GameObject> skillAreaEffectHighlights;
    protected List<Vector3> positionQueue;
    protected Quaternion targetRotation;
    protected Vector3 targetPosition;
    protected TileMapBuilder _tileMapBuilder;
    protected BattleGroundController _battleGroundController;
    protected List<UnitController> targets;
    protected List<Obstacle> obstaclesToAttack;
    protected string currentEffect;
    protected List<int> healthEffects;
    public List<Skill> skills;
    protected int skillChargingCounter;
    protected GameObject chargingEffect;
    //protected List<WeaponSkill> weaponSkills;


    // units parameters

    protected Unit associatedUnit; // associated Hero unit, if null then not essential character

    protected string unitName;
    protected int maxHealth;
    protected int maxMovement;
    protected int totalExperience;
    protected string className;

    protected int fireResistance;
    protected int freezeResistance;
    protected int poisonResistance;
    protected int defendBonus;


    public int health; //currentHealth

    protected float moveSpeed;
    protected int movesLeft;
    protected bool defending;


    public int getMaxHealth()
    {
        return maxHealth;
    }

    public string getUnitName()
    {
        return unitName;
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public Tile getUnitTile()
    {
        return TileMap.getTile((int)coordinates.x, (int)coordinates.z);
    }

    public void getAttacked(Weapon weapon, UnitController attacker, int bonusDamage)
    {
        if (audioSource)
        {
            audioSource.PlayOneShot(grunt);
        }
        if (anim != null)
        {
            if (this is PlayerUnitController)
            {
                anim.SetTrigger("hitTrigger");
                anim.SetFloat("hitSelect", 1);
            }
            else
            {
                anim.SetBool("isAttacked", true);
            }
        }
        float finalDamage = weapon.damage + bonusDamage + calculateFlankDamage(attacker, weapon);
        if (weapon.damageType.Equals("fire"))
        {
            if (fireResistance == 100)
            {
                return; //Should tell player that unit is resistant to fire
            }
            finalDamage -= finalDamage * (fireResistance * 0.01f);
        }
        if (weapon.damageType.Equals("poison"))
        {
            if (poisonResistance == 100)
            {
                return;
            }
            finalDamage -= finalDamage * (poisonResistance * 0.01f);
        }
        if (weapon.damageType.Equals("freeze"))
        {
            if (freezeResistance == 100)
            {
                return;
            }
            finalDamage -= finalDamage * (freezeResistance * 0.01f);
        }
        if (defending)
        {
            finalDamage -= finalDamage * 0.35f;
        }
        finalDamage -= defendBonus * 0.01f;
        health = health - (int)finalDamage;
        _battleGroundController.playerUIHealth.text = _battleGroundController.lastActiveUnit.health.ToString() + " HP";
        HealthPopUpController.createPopUpText(((int)finalDamage).ToString(), transform, true);
        if (health <= 0)
        {
            _battleGroundController.playerUIHealth.text = 0 + " HP";
            killUnit();
        }

        if (weapon.damageType.Equals("fire"))
        {
            if (currentEffect.Contains("freeze"))
            {
                currentEffect = "none";
            }
            else
            {
                healthEffects.Clear();
                for (int i = 0; i < weapon.nextTurnsDamage.Count; i++)
                {
                    float value = weapon.nextTurnsDamage[i];
                    value -= value * (fireResistance * 0.01f);
                    healthEffects.Add((int)value);
                }
            }
        }
        else if (weapon.damageType.Equals("freeze"))
        {

        }
        else if (weapon.damageType.Equals("poison"))
        {

        }
        if (weapon.visualEffect != null && !checkIfIsResitant(weapon.damageType))
        {
            UnityEngine.Object visualEffect = Resources.Load("Prefabs/" + weapon.visualEffect);
            currentVisualEffect = (GameObject)GameObject.Instantiate(visualEffect, new Vector3(positions[0].x, positions[0].y + 0.5f, positions[0].z), Quaternion.Euler(-90, 0, 0));
            currentVisualEffect.transform.parent = gameObject.transform;
            currentVisualEffect.transform.localScale = new Vector3(currentVisualEffect.transform.localScale.x * gameObject.transform.localScale.x, currentVisualEffect.transform.localScale.y * gameObject.transform.localScale.y, currentVisualEffect.transform.localScale.z * gameObject.transform.localScale.z);
        }
    }


    private bool checkIfIsResitant(string damageType)
    {
        if (fireResistance == 100 && damageType.Equals("fire"))
        {
            return true;
        }
        else if (freezeResistance == 100 && damageType.Equals("freeze"))
        {
            return true;
        }
        else if (poisonResistance == 100 && damageType.Equals("poison"))
        {
            return true;
        }
        return false;
    }

    private int calculateFlankDamage(UnitController attacker, Weapon weapon)
    {
        int damage = 0;
        if (weapon.isFlankingBonus)
        {
            if (facingDirection.Equals("up") || facingDirection.Equals("down"))
            {
                if ((attacker.coordinates.x < coordinates.x && facingDirection.Equals("up")) || (attacker.coordinates.x > coordinates.x && facingDirection.Equals("down")))
                {
                    damage = 8;
                }
                else if (attacker.coordinates.x == coordinates.x)
                {
                    damage = 4;
                }
            }
            else if (facingDirection.Equals("right") || facingDirection.Equals("left"))
            {
                if ((attacker.coordinates.z > coordinates.z && facingDirection.Equals("right")) || (attacker.coordinates.z < coordinates.z && facingDirection.Equals("left")))
                {
                    damage = 8;
                }
                else if (attacker.coordinates.z == coordinates.z)
                {
                    damage = 4;
                }
            }
        }
        return damage;
    }

    protected int getBonusDamageFromWeaponSkill()
    {
        return 0;

        //int bonusDamage = -1;                         //TODO not sure if weapon Skill even will be a thing, just abilities
        //for (int i = 0; i < weaponSkills.Count; i++)
        //{
        //    if (weaponSkills[i].name.Equals(currentItem.name))
        //    {
        //        bonusDamage = weaponSkills[i].level * 2;
        //    }
        //}
        //if (bonusDamage == -1)
        //{
        //    bonusDamage = 0;
        //    weaponSkills.Add(new WeaponSkill(currentItem.name));
        //}
        //return bonusDamage;
    }

    public void getHealed(HealingItem healingItem)           //TODO
    {
        int healing = 0;
        if (health + healingItem.healingPoints > maxHealth)
        {
            healing = maxHealth - health;
        }
        else
        {
            healing = healingItem.healingPoints;
        }
        health += healing;
        _battleGroundController.playerUIHealth.text = _battleGroundController.lastActiveUnit.health.ToString() + " HP";
        HealthPopUpController.createPopUpText(healing.ToString(), transform, false);
    }

    //protected void weaponSkillUpgrade()
    //{
    //    for (int i = 0; i < weaponSkills.Count; i++)
    //    {
    //        if (weaponSkills[i].name.Equals(currentItem.name))
    //        {
    //            weaponSkills[i].experience++;
    //            if (weaponSkills[i].experience >= weaponSkills[i].levelRequirements[weaponSkills[i].level])
    //            {
    //                weaponSkills[i].level++;
    //            }
    //        }
    //    }
    //}

    public void killUnit()
    {
        TileMap.setTileWalkable((int)coordinates.x, (int)coordinates.z);
        if (currentVisualEffect != null)
        {
            Destroy(currentVisualEffect);
        }
        if (this is Enemy)
        {
            _battleGroundController.enemyUnits.Remove((Enemy)this);
        }
        else if (this is PlayerUnitController)
        {
            _battleGroundController.playerUnits.Remove((PlayerUnitController)this);
        }
        if (anim != null)
        {
            if (this is PlayerUnitController)
            {
                //anim.SetTrigger("swordDeathTrigger");
            }
            else
            {
                anim.SetBool("isDeath", true);
            }
            StartCoroutine(waitForTimeBeforeDeath(6.85f));
        }
        else
        {
            Destroy(gameObject);
        }
    }

    protected GameObject createPlane(int x, int z, Color color)   // needs to create mesh from sratch for better performance TODO
    {
        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.transform.localScale = new Vector3(0.1f, 1.0f, 0.1f);
        plane.transform.position = new Vector3(x, 0.05f, z) * _tileMapBuilder.tileSize;
        plane.transform.position = new Vector3(plane.transform.position.x + 0.5f, plane.transform.position.y, plane.transform.position.z + 0.5f);
        plane.GetComponent<Renderer>().material.color = color;
        plane.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
        return plane;
    }

    protected void moveToNextStep(float offset)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, moveSpeed * 2.2f * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
        if (Vector3.Distance(targetPosition, transform.position) <= 0.01f)
        {
            coordinates = positionQueue[0];
            transform.position = targetPosition;
            setPositions();
            positionQueue.RemoveAt(0);
            moving = false;
        }
        if (positionQueue.Count == 0)
        {
            for (int i = 0; i < positions.Count; i++)
            {
                TileMap.setTileNotWalkable((int)positions[i].x, (int)positions[i].z - 1);
            }
            if (this is PlayerUnitController)
            {
                for (int i = 0; i < _battleGroundController.playerUnits.Count; i++)
                {
                    _battleGroundController.playerUnits[i].calculateDefendBonus(false);
                }
            }
        }
    }


    protected void setNextStep(Vector3[] rotations)
    {
        if (positionQueue[0].x > coordinates.x)
        {
            turnCorrectWay(facingDirection = "up", rotations);
            targetPosition = new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z);
            if (type == 3)
            {
                additionalPositions[0] = transform.position;
            }
        }
        else if (positionQueue[0].x < coordinates.x)
        {
            turnCorrectWay(facingDirection = "down", rotations);
            targetPosition = new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z);
            if (type == 3)
            {
                additionalPositions[0] = transform.position;
            }
        }
        else if (positionQueue[0].z < coordinates.z)
        {
            turnCorrectWay(facingDirection = "right", rotations);
            targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1f);
            if (type == 3)
            {
                additionalPositions[0] = transform.position;
            }
        }
        else if (positionQueue[0].z > coordinates.z)
        {
            turnCorrectWay(facingDirection = "left", rotations);
            targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1f);
            if (type == 3)
            {
                additionalPositions[0] = transform.position;
            }
        }
        moving = true;
    }

    protected void turnCorrectWay(string direction, Vector3[] rotations)
    {
        switch (direction)
        {
            case "up":
                targetRotation = Quaternion.Euler(rotations[0]);
                break;
            case "right":
                targetRotation = Quaternion.Euler(rotations[2]);
                break;
            case "down":
                targetRotation = Quaternion.Euler(rotations[1]);
                break;
            case "left":
                targetRotation = Quaternion.Euler(rotations[3]);
                break;
        }
    }

    protected void turnToEnemy(Skill currentSkill)
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, moveSpeed * 80f * Time.deltaTime);
        if (Quaternion.Angle(transform.rotation, targetRotation) < 5.0f)
        {
            transform.rotation = targetRotation;
            turningToTarget = false;
            if (currentSkill != null)
            {
                useSkill = true;
            }
            else
            {
                attack = true;
            }
        }
    }

    protected void setPositions()
    {
        positions.Clear();
        positions.Add(transform.position);
        if (additionalPositions != null)
        {
            for (int i = 0; i < additionalPositions.Count; i++)
            {
                positions.Add(additionalPositions[i]);
            }
        }
    }

    protected void calculateDefendBonus(bool visualEffect)
    {
        defendBonus = 0;
        List<Tile> nearbyTiles = TileMap.GetListOfAdjacentTiles((int)coordinates.x, (int)coordinates.z);
        for (int i = 0; i < nearbyTiles.Count; i++)
        {
            for (int j = 0; j < _battleGroundController.playerUnits.Count; j++)
            {
                if (_battleGroundController.playerUnits[j].className.Equals("Barbarian") || className.Equals("Barbarian"))
                {
                    //TODO change to read from skills with passive block bonus
                    if (nearbyTiles[i].Equals(_battleGroundController.playerUnits[j].getUnitTile()))
                    {
                        defendBonus += 13;
                    }
                }
                
            }
        }
        if (visualEffect && defendBonus > 0)
        {
            EffectPopUpController.createPopUp("Shield", defendBonus.ToString(), transform);
            _battleGroundController.checkDialogAction("Defense Bonus");
        }
    }

    protected void standardReset()
    {
        movesLeft = maxMovement;
    }

    public void checkHealthModifiers()
    {
        if (healthEffects.Count > 0)
        {
            isUpdatingHealth = true;
            StartCoroutine(waitForTimeBeforeHealthUpdate(1.5f));
        }
        else
        {
            isUpdatingHealth = false;
            isHealthUpdated = true;
        }
    }

    private void updateHealthModifiers()
    {
        health -= healthEffects[0];
        if (healthEffects[0] > 0)
        {
            if (anim != null)
            {
                if (health <= 0)
                {
                    anim.SetBool("isDeath", true);
                }
                else
                {
                    anim.SetBool("isAttacked", true);
                }
            }
        }
        else
        {
            if (anim != null)
            {
                //anim.Play("Healed"); TODO
            }
        }
        HealthPopUpController.createPopUpText(healthEffects[0].ToString(), transform, true);
        healthEffects.RemoveAt(0);
        if (!(healthEffects.Count > 0) && currentVisualEffect != null)
        {
            Destroy(currentVisualEffect);
        }
        StartCoroutine(waitForTimeBeforeHealthUpdated(1.5f));
    }

    private void die()
    {
        if (currentVisualEffect != null)
        {
            Destroy(currentVisualEffect);
        }
        Destroy(gameObject); //maybe dead models should stay on map TODO
    }

    IEnumerator waitForTimeBeforeDeath(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        die();
    }

    IEnumerator waitForTimeBeforeHealthUpdate(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        updateHealthModifiers();
    }

    IEnumerator waitForTimeBeforeHealthUpdated(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        isHealthUpdated = true;
        isUpdatingHealth = false;
        if (health <= 0)
        {
            killUnit();
        }
    }


    bool isAnimRunning(Animator animator)
    {
        for (int i = 0; i < animator.layerCount; i++)
        {
            AnimatorClipInfo[] nextAnim = animator.GetNextAnimatorClipInfo(i);
            if (nextAnim.Length > 0 && nextAnim[0].clip.isLooping == false)
            {
                return true;
            }


            AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(i);
            if (info.loop == false && info.normalizedTime < 0.95)
            {
                return true;
            }
        }

        return false;
    }

}
