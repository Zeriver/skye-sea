﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    private float cameraSpeed;
    private bool movingToActive;
    private Vector3 target;
    private TileMapBuilder _tileMapBuilder;

    private float minCameraHeight;
    private float maxCameraHeight;
    private float cameraEdgeSpeed, cameraEdgeOffset;
    private bool isCameraBlocked;

    private Vector3 mapCenter;

    void Start()
    {
        movingToActive = false;
        cameraSpeed = 3.0f;
        Camera.main.transform.rotation = Quaternion.Euler(37.5f, 47.0f, 0.0f);
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, 7.0f, Camera.main.transform.position.z);

        minCameraHeight = 6.0f;
        maxCameraHeight = 18.0f;
        cameraEdgeOffset = 5.0f;
        cameraEdgeSpeed = 0.2f;

        _tileMapBuilder = GetComponent<TileMapBuilder>();

        mapCenter = new Vector3(_tileMapBuilder.size_x / 2, Camera.main.transform.position.y, -_tileMapBuilder.size_z / 2);
        Camera.main.transform.position = new Vector3(0.3518806f, 7.0f, -18.14436f); //TODO different for each battlegrounds?
    }


    void Update()
    {
        // Camera needs to follow unit if it is on the edge of camera vision or outside view TODO

        if (isCameraBlocked)
        {
            return;
        }

        Vector3 oldPos = Camera.main.transform.position;
        if (movingToActive)
        {
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, target, cameraSpeed * Time.deltaTime);
            float distance = Vector3.Distance(Camera.main.transform.position, target);
            if (distance < 0.5f)
            {
                movingToActive = false;
            }
            if (Input.GetMouseButtonDown(1))
            {
                Camera.main.transform.position = target;
                movingToActive = false;
            }
        }

        //Mouse on edge
        if (Input.mousePosition.x >= Screen.width - cameraEdgeOffset || Input.GetKey("right") || Input.GetKey(KeyCode.D))
        {
            Camera.main.transform.position = Camera.main.transform.position + new Vector3(cameraEdgeSpeed, 0.0f, -cameraEdgeSpeed);
            movingToActive = false;
        }
        if (Input.mousePosition.x <= 0 + cameraEdgeOffset || Input.GetKey("left") || Input.GetKey(KeyCode.A))
        {
            Camera.main.transform.position = Camera.main.transform.position + new Vector3(-cameraEdgeSpeed, 0.0f, cameraEdgeSpeed);
            movingToActive = false;
        }
        if (Input.mousePosition.y >= Screen.height - cameraEdgeOffset || Input.GetKey("up") || Input.GetKey(KeyCode.W))
        {
            Camera.main.transform.position = Camera.main.transform.position + new Vector3(cameraEdgeSpeed, 0.0f, cameraEdgeSpeed);
            movingToActive = false;
        }
        if (Input.mousePosition.y <= 0 + cameraEdgeOffset || Input.GetKey("down") || Input.GetKey(KeyCode.S))
        {
            Camera.main.transform.position = Camera.main.transform.position + new Vector3(-cameraEdgeSpeed, 0.0f, -cameraEdgeSpeed);
            movingToActive = false;
        }
        if (Camera.main.transform.position.x > _tileMapBuilder.size_x / 1.4f || Camera.main.transform.position.x < -2
            || Camera.main.transform.position.z > -5 || Camera.main.transform.position.z < -_tileMapBuilder.size_z)
        {
            Camera.main.transform.position = oldPos;
            movingToActive = false;
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0f)
        {
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y + (Input.GetAxis("Mouse ScrollWheel") * 10), Camera.main.transform.position.z);
            movingToActive = false;
            if (Camera.main.transform.position.y > maxCameraHeight)
            {
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, maxCameraHeight, Camera.main.transform.position.z);
            }
            else if (Camera.main.transform.position.y < minCameraHeight)
            {
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, minCameraHeight, Camera.main.transform.position.z);
            }
            mapCenter = new Vector3(mapCenter.x, Camera.main.transform.position.y, mapCenter.z);
        }
    }

    public void setCameraBlock(bool value)
    {
        isCameraBlocked = value;
        Cursor.visible = !value;
    }

    public void setCameraToActiveUnit(Vector3 unitPosition)
    {
        target = new Vector3(unitPosition.x - 7.5f, Camera.main.transform.position.y, unitPosition.z - 7.5f);
        movingToActive = true;
    }

}
