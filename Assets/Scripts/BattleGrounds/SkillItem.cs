﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SkillItem : MonoBehaviour
{

    public RawImage image;
    public Text skillName;
    public Text description;

    private PlayerUnitController unit;
    private bool isFlashingElement;
    private RawImage flashingElement;

    void Start()
    {
        
    }


    void Update()
    {
        if (isFlashingElement && flashingElement != null)
        {
            Color lerpedColor = Color.Lerp(Color.yellow, Color.magenta, Mathf.PingPong(Time.time, 1));
            flashingElement.color = lerpedColor;
        }
    }

    public void setSkillInfo(Skill skill, PlayerUnitController unit)
    {
        skillName.text = skill.Name;
        description.text = skill.Description;
        this.unit = unit;
        if (skill.CurrentCooldown > 0)
        {
            GetComponent<Button>().interactable = false;
            description.text += " On cooldown: " + skill.CurrentCooldown;
        }
        else
        {
            description.text += " Cooldown: " + skill.Cooldown;
        }
    }

    public void startFlashing()
    {
        isFlashingElement = true;
        flashingElement = GetComponent<RawImage>();
    }

    public void useSkill()
    {
        unit.skillMode(skillName.text);
    }

}
