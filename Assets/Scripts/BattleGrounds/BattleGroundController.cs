﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;
using System;
using System.Linq;

[RequireComponent(typeof(TileMapBuilder))]
[RequireComponent(typeof(MouseHighlight))]
[RequireComponent(typeof(CameraController))]
public class BattleGroundController : MonoBehaviour
{
    TileMapBuilder _tileMapBuilder;
    MouseHighlight _mouseHighlight;
    CameraController _cameraController;


    private AudioSource audioSource;

    public GameObject _playerUI;
    public GameObject _dialogCanvas;
    public GameObject _unitInfo;
    public GameObject _menu;
    public Text playerUIHealth;


    public AudioClip playerMusic;
    public AudioClip enemyMusic;


    public List<PlayerUnitController> playerUnits = new List<PlayerUnitController>();
    public List<Enemy> enemyUnits = new List<Enemy>();
    public PlayerUnitController lastActiveUnit;


    private PlayerUI playerUI;
    private UnitInfoPanel unitInfo;
    private BattleDialogController dialogController;
    private MenuController menuController;
    public bool playerUnitActionsBlocked;

    private bool enemyTurn, allyTurn, endTurn, enemyHealthUpdate, playerHealthUpdate, initializeNewEnemy, newEnemiesInitialized, dialog, enteringBattleGrounds, playerScriptedAction;
    public bool playerTurn;
    private int turnNumber;

    private string[][] dialogs;
    private string dialogCache = "empty";

    private string[][] playerScripted;
    private string playerScriptedCache = "empty";
    private bool characterPerformedAction;

    UnityEngine.Object _playerUnit;
    UnityEngine.Object _mog;
    UnityEngine.Object _eiger;
    UnityEngine.Object _glory;
    UnityEngine.Object _blitz;
    UnityEngine.Object _dietrich;


    //Enemy units
    UnityEngine.Object _bandit;



    void Start()
    {
        _tileMapBuilder = GetComponent<TileMapBuilder>();
        _mouseHighlight = GetComponent<MouseHighlight>();
        _cameraController = GetComponent<CameraController>();
        audioSource = GetComponent<AudioSource>();

        _playerUnit = Resources.Load("Prefabs/PlayerUnit");
        _mog = Resources.Load("Prefabs/Mog");
        _eiger = Resources.Load("Prefabs/Eiger");
        _glory = Resources.Load("Prefabs/Glory");
        _blitz = Resources.Load("Prefabs/Blitz");
        _dietrich = Resources.Load("Prefabs/Dietrich");
        _bandit = Resources.Load("Prefabs/Bandit");


        unitInfo = _unitInfo.GetComponent("UnitInfoPanel") as UnitInfoPanel;
        dialogController = _dialogCanvas.GetComponent("BattleDialogController") as BattleDialogController;
        playerUI = _playerUI.GetComponent("PlayerUI") as PlayerUI;
        menuController = _menu.GetComponent("MenuController") as MenuController;

        Debug.Log("Creating Map");
        string fileLevel = GameManager.instance.getCurrentBattleMap();
        string[][] mapInfo = FileReader.readMapDialogs(Application.dataPath + "/Maps/" + fileLevel + "/mapInfo.txt"); // TODO LATER GET MORE MAP INFO

        List<string> mapSize = mapInfo[0][0].Split(',').ToList<string>();
        createBattleGround(int.Parse(mapSize[0]), int.Parse(mapSize[1]), GameManager.instance.getMissionUnits(), fileLevel);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            menuWindowService();
        }
        if (Input.GetKeyDown(KeyCode.Space) && dialogController.canvas.enabled)
        {
            dialog = true;
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("updating hero units and back to world view");
            for (int i = 0; i < playerUnits.Count; i++)
            {
                playerUnits[i].updateHeroUnitsStats();
            }
            SceneManager.LoadScene("WorldViewBase");
            //getDialogForCode("T1");
        }
        if (enteringBattleGrounds)
        {
            bool allUnitsEnteredPostion = true;
            for (int i = 0; i < playerUnits.Count; i++)
            {
                if (playerUnits[i].enteringPosition)
                {
                    allUnitsEnteredPostion = false;
                }
            }
            if (allUnitsEnteredPostion)
            {
                enteringBattleGrounds = false;
                dialog = true;
                setCameraMovementBlock(false);
            }
        }
        if (dialog)
        {
            if (!checkDialogs())
            {
                dialog = false;
                if (dialogCache.Equals("empty"))
                {
                    playerScriptedAction = true;
                    characterPerformedAction = true;
                }
            }
        }
        if (playerScriptedAction)
        {
            if (!checkPlayerScriptedActions() && characterPerformedAction)
            {
                playerScriptedAction = false;
                lastActiveUnit.setPlayerUnitActive();
                _cameraController.setCameraToActiveUnit(lastActiveUnit.transform.position);
                playerHealthUpdate = true;
            }
        }
        if (!menuController.menu.enabled && !dialogController.canvas.enabled && !playerScriptedAction)
        {
            if (playerHealthUpdate)
            {
                for (int i = 0; i < playerUnits.Count; i++)
                {
                    playerUnits[i].checkHealthModifiers();
                }
                playerHealthUpdate = false;
                playerTurn = true;
                enemyHealthUpdate = false;
                enemyTurn = false;
            }
            if (playerTurn && !lastActiveUnit.moving)
            {
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    inventoryWindowService();
                }
                if (true) //!inventory.equipment.enabled
                {
                    if (Input.GetKeyDown(KeyCode.Tab))
                    {
                        setNextUnitActive();
                    }
                    Transform mousePosition = _mouseHighlight.getHighlightSelection();
                    if (Input.GetMouseButtonDown(0) && !playerUnitActionsBlocked)     /// LEFT CLICK
                    {
                        if (lastActiveUnit.giveHighlights.Count == 0 && !lastActiveUnit.isActionMode)
                        {
                            PlayerUnitController clickedUnit = getClickedUnit((int)mousePosition.position.x, (int)mousePosition.position.z);
                            if (clickedUnit != null)
                            {
                                deactivatePlayerUnits();
                                clickedUnit.setPlayerUnitActive();
                                lastActiveUnit = clickedUnit;
                                playerUI.setUnitInfo(lastActiveUnit);
                                _cameraController.setCameraToActiveUnit(clickedUnit.transform.position);
                            }
                            Tile clickedTile = TileMap.getTile((int)mousePosition.position.x, (int)mousePosition.position.z);
                        }
                    }
                    if (!lastActiveUnit.isActionMode && lastActiveUnit.giveHighlights.Count == 0 && lastActiveUnit.pushHighlights.Count == 0)
                    {
                        Enemy enemy = getHoveredEnemy((int)mousePosition.position.x, (int)mousePosition.position.z);
                        if (enemy != null)
                        {
                            lastActiveUnit.destroyMovementHiglights();
                            enemy.showPossibleMovement();
                            if (!unitInfo.canvas.enabled)
                            {
                                unitInfo.setNewPosition(enemy.transform.position);
                                unitInfo.setInfo(enemy);
                                unitInfo.changeCanvasEnabled(true);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < enemyUnits.Count; i++)
                            {
                                enemyUnits[i].destroyMovementHighlights();
                            }
                            if (unitInfo.canvas.enabled)
                            {
                                unitInfo.changeCanvasEnabled(false);
                                lastActiveUnit.showAllowedMovements();
                            }
                        }
                    }
                }
            }
            if (enemyHealthUpdate)
            {
                for (int i = 0; i < enemyUnits.Count; i++)
                {
                    if (!enemyUnits[i].isUpdatingHealth && !enemyUnits[i].isHealthUpdated)
                    {
                        if (i > 0 && enemyUnits[i - 1].isHealthUpdated)
                        {
                            _cameraController.setCameraToActiveUnit(enemyUnits[i].transform.position);
                            enemyUnits[i].checkHealthModifiers();
                        }
                        else if (i == 0)
                        {
                            _cameraController.setCameraToActiveUnit(enemyUnits[i].transform.position);
                            enemyUnits[i].checkHealthModifiers();
                        }
                    }
                }
                bool allEnemiesHealthUpdated = true;
                for (int i = 0; i < enemyUnits.Count; i++)
                {
                    if (!enemyUnits[i].isHealthUpdated)
                    {
                        allEnemiesHealthUpdated = false;
                    }
                }
                if (allEnemiesHealthUpdated)
                {
                    enemyHealthUpdate = false;
                    enemyTurn = true;
                }
            }
            if (enemyTurn)
            {
                performEnemyTurn();
                bool allEnemiesDone = true;
                //Debug.Log("Enemy turn in progress...");
                for (int i = 0; i < enemyUnits.Count; i++)
                {
                    if (!enemyUnits[i].turnDone)
                    {
                        allEnemiesDone = false;
                    }
                }
                if (allEnemiesDone)
                {
                    enemyTurn = false;
                    allyTurn = true;
                }
            }
            if (checkLoseCondition())
            {
                // reset / end game TODO
            }
            if (allyTurn)
            {
                allyTurn = false;
                initializeNewEnemy = true;
            }
            if (checkWinCondition())
            {
                // end game TODO
            }
            if (initializeNewEnemy)
            {
                if (enterNewEnemies())
                {
                    initializeNewEnemy = false;
                    endTurn = true;
                }
            }
            if (endTurn)
            {
                nextTurn();
            }
        }
    }

    public void createBattleGround(int wdith, int height, List<Unit> playerUnitsToLoad, string fileLevel)
    {
        turnNumber = 1;
        _tileMapBuilder.BuildMesh(wdith, height, fileLevel);
        dialogs = FileReader.readMapDialogs(Application.dataPath + "/Maps/" + fileLevel + "/dialog.txt");
        playerScripted = FileReader.readMapDialogs(Application.dataPath + "/Maps/" + fileLevel + "/ScriptedPlayerActions.txt");
        string[][] playerStartingPositions = FileReader.readMapDialogs(Application.dataPath + "/Maps/" + fileLevel + "/startingPositions.txt");
        string[][] enemyStartingPositions = FileReader.readMapDialogs(Application.dataPath + "/Maps/" + fileLevel + "/startingPositionsEnemy.txt");

        //Create units (player units and enemies)

        for (int i = 0; i < playerUnitsToLoad.Count; i++)
        {
            GameObject playerUnit = null;
            if (playerUnitsToLoad[i].getName().Equals("Mog"))
            {
                playerUnit = (GameObject)GameObject.Instantiate(_mog);
            }
            else if (playerUnitsToLoad[i].getName().Equals("Eiger"))
            {
                playerUnit = (GameObject)GameObject.Instantiate(_eiger);
            }
            else if (playerUnitsToLoad[i].getName().Equals("Glory"))
            {
                playerUnit = (GameObject)GameObject.Instantiate(_glory);
            }
            else if (playerUnitsToLoad[i].getName().Equals("Blitz"))
            {
                playerUnit = (GameObject)GameObject.Instantiate(_blitz);
            }
            else if (playerUnitsToLoad[i].getName().Equals("Dietrich"))
            {
                playerUnit = (GameObject)GameObject.Instantiate(_dietrich);
            }
            else
            {
                playerUnit = (GameObject)GameObject.Instantiate(_playerUnit);
            }
            playerUnits.Add(playerUnit.GetComponent<PlayerUnitController>());

            List<string> positionInfo = playerStartingPositions[i][0].Split(',').ToList<string>();
            playerUnits[i].createPlayerUnit(int.Parse(positionInfo[0]), int.Parse(positionInfo[1]), positionInfo[2], playerUnitsToLoad[i]);
            playerUnits[i].setEnteringPosition(int.Parse(positionInfo[3]), int.Parse(positionInfo[4]));

            if (i == 0)
            {
                lastActiveUnit = playerUnits[i];
            }
        }

        setCameraMovementBlock(true);
        playerUI.IsOpen = false;

        for (int i = 0; i < enemyStartingPositions.Length; i++)
        {
            List<string> positionInfo = enemyStartingPositions[i][0].Split(',').ToList<string>();

            enemyUnits.Add(((GameObject)GameObject.Instantiate(_bandit)).GetComponent<Bandit>());
            enemyUnits[i].createBandit(int.Parse(positionInfo[0]), int.Parse(positionInfo[1]), positionInfo[2]);
        }

        StartCoroutine(delayedUnitActivation());
        _cameraController.setCameraToActiveUnit(lastActiveUnit.transform.position);
        HealthPopUpController.Initialize();
        EffectPopUpController.Initialize();
        characterPerformedAction = true;
        enteringBattleGrounds = true;
        //playerHealthUpdate = true;
        audioSource.clip = playerMusic;
        audioSource.Play();
    }

    private bool checkWinCondition() // add more conditions based on mission objectives TODO
    {
        if (enemyUnits.Count == 0)
        {
            return true;
        }
        return false;
    }

    private bool checkLoseCondition() // add more conditions based on mission objectives TODO
    {
        if (playerUnits.Count == 0)
        {
            return true;
        }
        return false;
    }

    public void performPlayerAction()
    {
            playerScriptedAction = true;
    }

    private bool checkPlayerScriptedActions()
    {
        if (playerScriptedCache != null && !playerScriptedCache.Equals("empty"))
        {
            int charLocation = playerScriptedCache.IndexOf(";", StringComparison.Ordinal);
            if (charLocation > 0)
            {
                string[] texts = Regex.Split(playerScriptedCache.Substring(0, charLocation), ":");

                for (int i = 0; i < playerUnits.Count; i++)
                {
                    if (texts[0].Equals(playerUnits[i].getUnitName()))
                    {
                        playerUI.setButtonsInteractable(true, "");
                        string[] coordinates = Regex.Split(texts[1], ",");
                        if (coordinates.Length == 1)
                        {
                            playerUI.weaponFlashing(playerUnits[i], coordinates[0]);
                            playerUnits[i].setPlayerUnitActive();
                        }
                        else if (coordinates.Length > 2)
                        {
                            playerUnits[i].setPlayerUnitSkillActive(coordinates[0], int.Parse(coordinates[1]), int.Parse(coordinates[2]));
                            playerUI.skillFlashing(coordinates[0]);
                            playerUI.setButtonsInteractable(false, "Skills");
                        }
                        else
                        {
                            playerUnits[i].setPlayerUnitActive(int.Parse(coordinates[0]), int.Parse(coordinates[1]));
                            playerUI.setButtonsInteractable(false, "");
                        }
                        _cameraController.setCameraToActiveUnit(playerUnits[i].transform.position);
                        lastActiveUnit = playerUnits[i];
                        playerUI.setUnitInfo(lastActiveUnit);
                        break;
                    }
                }
                characterPerformedAction = false;
                playerScriptedCache = playerScriptedCache.Remove(0, playerScriptedCache.Substring(0, charLocation).Length + 1);
                playerScriptedAction = false;
                return false;
            }
            else
            {
                playerScriptedCache = "empty";
                playerScriptedAction = true;
                characterPerformedAction = true;
                playerUI.setButtonsInteractable(true, "");
                return false;
            }
        }
        else
        {
            for (int i = 0; i < playerScripted.GetLength(0); i++)
            {
                if (playerScripted[i][0].Equals(turnNumber.ToString()))
                {
                    playerScripted[i][0] += "- READ";
                    playerScriptedCache = playerScripted[i][1];
                    lastActiveUnit.deactivatePlayerUnit();
                    checkPlayerScriptedActions();
                }
            }
        }
        return false;
    }

    public void setDialogEvent(string dialogText)
    {
        dialogCache = dialogText;
        dialogController.changeDialogCanvasEnabled();
        playerUI.IsOpen = false;
        lastActiveUnit.deactivatePlayerUnit();
        dialog = true;
    }

    private bool checkDialogs()
    {
        if (dialogCache != null && !dialogCache.Equals("empty"))
        {
            int charLocation = dialogCache.IndexOf(";", StringComparison.Ordinal);
            if (charLocation > 0)
            {
                dialogController.setNewText(dialogCache.Substring(0, charLocation));
                string unitName = Regex.Split(dialogCache.Substring(0, charLocation), ":")[0];
                dialogCache = dialogCache.Remove(0, dialogCache.Substring(0, charLocation).Length + 1);
                lastActiveUnit.deactivatePlayerUnit();
                for (int i = 0; i < playerUnits.Count; i++)
                {
                    if (playerUnits[i].getUnitName().Equals(unitName))
                    {
                        _cameraController.setCameraToActiveUnit(playerUnits[i].transform.position);
                    }
                }
                return false;
            }
            else
            {
                dialogCache = "empty";
                dialogController.changeDialogCanvasEnabled();
                playerUI.IsOpen = true;
                return false;
            }
        }
        else
        {
            for (int i = 0; i < dialogs.GetLength(0); i++)
            {
                if (dialogs[i][0].Equals(turnNumber.ToString()))
                {
                    dialogs[i][0] += "- READ";
                    dialogCache = dialogs[i][1];
                    dialogController.changeDialogCanvasEnabled();
                    playerUI.IsOpen = false;
                    lastActiveUnit.deactivatePlayerUnit();
                    checkDialogs();
                }
            }
        }
        return false;
    }

    private void getDialogForCode(string code)
    {
        for (int i = 0; i < dialogs.GetLength(0); i++)
        {
            if (dialogs[i][0].Equals(code))
            {
                dialogs[i][0] += "- READ";
                dialogCache = dialogs[i][1];
                dialogController.changeDialogCanvasEnabled();
                playerUI.IsOpen = false;
                checkDialogs();
            }
        }
        Debug.Log("Warning: dialog code not found!");
    }

    private bool enterNewEnemies()
    {
        //if (!newEnemiesInitialized)
        //{
        //    if (turnNumber == 2)
        //    {
        //        newEnemiesInitialized = true;
        //        enemyUnits.Add(Instantiate(_impaler).GetComponent<Impaler>());
        //        enemyUnits[enemyUnits.Count - 1].createImpaler(2, 2, "down");
        //        enemyUnits[enemyUnits.Count - 1].enterBattleground(4, 13);
        //    }
        //    if (turnNumber == 3)
        //    {
        //        newEnemiesInitialized = true;
        //        enemyUnits.Add(Instantiate(_impaler).GetComponent<Impaler>());
        //        enemyUnits[enemyUnits.Count - 1].createImpaler(2, 2, "down");
        //        enemyUnits[enemyUnits.Count - 1].enterBattleground(4, 13);
        //        //enemyUnits.Add(Instantiate(_impaler).GetComponent<Impaler>());
        //        //enemyUnits[enemyUnits.Count - 1].createImpaler(4, 4, "down");
        //        //enemyUnits[enemyUnits.Count - 1].enterBattleground(4, 25);
        //    }
        //}

        bool newEnemiesReady = true;
        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (!enemyUnits[i].turnDone)
            {
                newEnemiesReady = false;
            }
        }
        return newEnemiesReady;
    }

    private void setNextUnitActive()
    {
        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (playerUnits[i].isSelected)
            {
                playerUnits[i].deactivatePlayerUnit();
                if (i + 1 != playerUnits.Count)
                {
                    playerUnits[i + 1].setPlayerUnitActive();
                    lastActiveUnit = playerUnits[i + 1];
                    _cameraController.setCameraToActiveUnit(playerUnits[i + 1].transform.position);
                    playerUI.setUnitInfo(lastActiveUnit);
                    break;
                }
                else
                {
                    playerUnits[0].setPlayerUnitActive();
                    lastActiveUnit = playerUnits[0];
                    _cameraController.setCameraToActiveUnit(playerUnits[0].transform.position);
                    playerUI.setUnitInfo(lastActiveUnit);
                    break;
                }
            }
        }
    }

    public void weaponSwitch()
    {
        if(!lastActiveUnit.moving)
        {
            string weapon = lastActiveUnit.switchWeapon();
            if (!weapon.Equals("Empty"))
            {
                playerUI.setCurrentItem(weapon);
            }
            playerUI.weaponAction();
        }
    }

    public void changeSkillMode()
    {
        if (!lastActiveUnit.moving && !lastActiveUnit.isActionUsed)
        {
            playerUI.changeSkillPanel(lastActiveUnit);
            playerUnitActionsBlocked = !playerUnitActionsBlocked;
        }
    }

    public void changeBeltMode()
    {
        if (!lastActiveUnit.moving && !lastActiveUnit.isActionUsed)
        {
            playerUI.changeBeltPanel(lastActiveUnit);
            playerUnitActionsBlocked = !playerUnitActionsBlocked;
        }
    }

    public void changeGiveMode()
    {
        if (!lastActiveUnit.moving)
        {
            lastActiveUnit.giveMode();
        }
    }

    public void changePushMode()
    {
        if (!lastActiveUnit.moving && !lastActiveUnit.isActionUsed)
        {
            lastActiveUnit.pushMode();
        }
    }

    public void defendPosition()
    {
        if (!lastActiveUnit.moving && !lastActiveUnit.isActionUsed)
        {
            lastActiveUnit.defendingPosition();
        }
    }

    private void deactivatePlayerUnits()
    {
        for (int i = 0; i < playerUnits.Count; i++)
        {
            if (playerUnits[i].isSelected)
            {
                playerUnits[i].deactivatePlayerUnit();
            }
        }
    }

    public void switchPlayerUnitMode()
    {
        if (!lastActiveUnit.moving && !lastActiveUnit.isActionUsed)
        {
            lastActiveUnit.switchActionMode();
            playerUI.weaponAction();
        }
    }

    private PlayerUnitController getClickedUnit(int x, int z)
    {
        PlayerUnitController unit = null;
        for (int i = 0; i < playerUnits.Count; i++)
        {
            if ((int)playerUnits[i].transform.position.x == x && (int)playerUnits[i].transform.position.z == z + 1) //+1 hack beacuse float is rounded to bigger value. Must improve TODO
            {
                unit = playerUnits[i];
                lastActiveUnit = unit;
            }
        }
        return unit;
    }

    private Enemy getHoveredEnemy(int x, int z)
    {
        Enemy unit = null;
        for (int i = 0; i < enemyUnits.Count; i++)
        {
            for (int j = 0; j < enemyUnits[i].positions.Count; j++)
            {
                if ((int)enemyUnits[i].positions[j].x == x && (int)enemyUnits[i].positions[j].z == z + 1) //+1 hack beacuse float is rounded to bigger value. Must improve TODO
                {
                    unit = enemyUnits[i];
                    break;
                }
            }
        }
        return unit;
    }

    public void nextTurn()
    {
        endTurn = false;
        newEnemiesInitialized = false;
        for (int i = 0; i < playerUnits.Count; i++)
        {
            playerUnits[i].resetAfterTurn();
            if (playerUnits[i].useSkill)
            {
                lastActiveUnit = playerUnits[i]; //Starting turn with unit that is ready to cast charged spell. Not sure if rework or remove
            }
        }
        if (lastActiveUnit.health <= 0)
        {
            if (playerUnits.Count != 0)
            {
                lastActiveUnit = playerUnits[0];
            }
        }
        lastActiveUnit.setPlayerUnitActive();
        _cameraController.setCameraToActiveUnit(lastActiveUnit.transform.position);
        for (int i = 0; i < enemyUnits.Count; i++)
        {
            enemyUnits[i].resetAfterTurn();
        }
        playerHealthUpdate = true;
        dialog = true;
        turnNumber++;
        audioSource.clip = playerMusic;
        audioSource.Play();
        playerUI.updateTurn(turnNumber);
        playerUI.IsOpen = true;
        playerUI.setUnitInfo(lastActiveUnit);
    }

    public void endPlayerTurn()
    {
        if (!lastActiveUnit.moving)
        {
            deactivatePlayerUnits();
            playerTurn = false;
            enemyHealthUpdate = true;
            playerUI.IsOpen = false;
            audioSource.clip = enemyMusic;
            audioSource.time = 0.0f;
            audioSource.Play();
        }
    }


    public void inventoryWindowService()
    {
        //if (!inventory.equipment.enabled)
        //{
        //    if (lastActiveUnit.isActionMode)
        //    {
        //        lastActiveUnit.switchActionMode();
        //    }
        //    itemCreator.createItems(lastActiveUnit.weapons.ConvertAll(x => (Item)x));
        //}
        //else
        //{
        //    itemCreator.destroyItems();
        //    lastActiveUnit.showAllowedMovements();
        //}
        //inventory.changeCanvasEnabled();
    }

    public void menuWindowService()
    {
        menuController.changeCanvasEnabled();
    }

    public void restartService()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void saveGameService()
    {

    }

    public void loadGameService()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void showWeapons()
    {
        //if (!itemCreator.isWeapon)
        //{
        //    itemCreator.destroyItems();
        //    itemCreator.createItems(lastActiveUnit.weapons.ConvertAll(x => (Item)x));
        //}
    }

    public void showHealingItems()
    {
        //if (!itemCreator.isHealingItem)
        //{
        //    itemCreator.destroyItems();
        //    itemCreator.createItems(lastActiveUnit.healingItems.ConvertAll(x => (Item)x));
        //}
    }

    public void blockUIActionsForUnit(string actionName)
    {
        playerUI.setButtonsInteractable(false, "EndTurn");
        checkDialogAction(actionName);
    }

    public void unlockUIElements()
    {
        playerUI.setButtonsInteractable(true, "");
    }

    public void checkDialogAction(string actionName)
    {
        for (int i = 0; i < dialogs.GetLength(0); i++)
        {
            string[] texts = Regex.Split(dialogs[i][0], ":");
            if (texts.Length > 1 && (texts[0].Equals("Any") || texts[0].Equals(lastActiveUnit.getUnitName())) && texts[1].Equals(actionName))
            {
                dialogs[i][0] += "- READ";
                setDialogEvent(dialogs[i][1]);
            }
        }
    }

    public void setCameraMovementBlock(bool value)
    {
        _cameraController.setCameraBlock(value);
        _mouseHighlight.setMeshVisibility(!value);
    }

    IEnumerator delayedUnitActivation()
    {
        yield return new WaitForSeconds(2.0f);
        lastActiveUnit.setPlayerUnitActive();
        playerUI.setUnitInfo(lastActiveUnit);
    }


    private void performEnemyTurn()
    {
        for (int i = 0; i < enemyUnits.Count; i++)
        {
            if (!enemyUnits[i].turnInProgress)
            {
                if (i > 0 && enemyUnits[i - 1].turnDone)
                {
                    _cameraController.setCameraToActiveUnit(enemyUnits[i].transform.position);
                    enemyUnits[i].performTurn();
                }
                else if (i == 0)
                {
                    _cameraController.setCameraToActiveUnit(enemyUnits[i].transform.position);
                    enemyUnits[i].performTurn();
                }
            }
        }
    }

}
