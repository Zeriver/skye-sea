﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class WorldEvent : MonoBehaviour
{

    public string eventID;
    public bool isAlwaysInteractive;
    public string missionName;
    public string missionDescription;
    public string nextEventLocation;
    public string[] missionScenes;
    public string[] dialogIds;

    private bool isShowAnimation;

    void Start()
    {

    }

    void Update()
    {
        transform.LookAt(2 * transform.position - Camera.main.transform.position);

        if (isShowAnimation)
        {
            transform.localScale += new Vector3(0.8f, 0.8f, 0.0f) * Time.deltaTime;
            if (transform.localScale.x >= 1)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
                isShowAnimation = false;
            }
        }
    }

    public void showEvent()
    {
        List<Mission> allMissions = new List<Mission>(GameManager.instance.getWorld().getCurrentMissions().Count + GameManager.instance.getWorld().getCompletedMissions().Count);
        allMissions.AddRange(GameManager.instance.getWorld().getCurrentMissions());
        allMissions.AddRange(GameManager.instance.getWorld().getCompletedMissions());

        for (int i = 0; i < allMissions.Count; i++)
        {
            if (allMissions[i].getMissionID().Equals(eventID))
            {
                return;
            }
        }
        isShowAnimation = true;
    }

    public void performEvent()
    {
        isShowAnimation = false;
        GameObject.Find("DialogCanvas").transform.GetChild(0).gameObject.SetActive(true);
        if (missionName != null && !missionName.Equals(""))
        {
            GameObject.Find("DialogCanvas").GetComponent<DialogController>().setAssignedMission(new Mission(eventID, missionName, missionDescription, nextEventLocation, missionScenes, dialogIds));
        }
        else // one time dialog only
        {
            if (!isAlwaysInteractive)
            {
                GameManager.instance.getWorld().getCompletedMissions().Add(new Mission(eventID));
            }
        }
        GameObject.Find("DialogCanvas").GetComponent<DialogController>().setDialogs(eventID);
        if (!isAlwaysInteractive)
        {
            transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
        }
    }
}
