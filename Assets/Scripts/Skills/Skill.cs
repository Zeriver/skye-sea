﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill
{
    protected string name;
    protected string description;
    protected int levelRequired;
    protected string classRequired;
    protected int range;
    protected List<Vector3> areOfEffect;
    protected string skillType;
    protected int unitCastingStun;
    protected int cooldown;
    protected int currentCooldown;
    protected string castingType;

    public abstract void useSkill(Tile tile); //TODO different types with different parameters



    public List<Tile> getAreaEffect(int x, int z, int x2, int z2) //refactoring TODO
    {
        List<Tile> area = new List<Tile>();
        if (name.Equals("Pistol") || name.Equals("Axe"))
        {
            area.Add(TileMap.getTile(x2, z2));
        }
        else if (name.Equals("Force Field"))
        {
            area = (TileMap.GetListOfAdjacentTiles(x2, z2));
        }

        return area;
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public string Description
    {
        get { return description; }
        set { description = value; }
    }

    public int LevelRequired
    {
        get { return levelRequired; }
        set { levelRequired = value; }
    }

    public string ClassRequired
    {
        get { return classRequired; }
        set { classRequired = value; }
    }

    public int Range
    {
        get { return range; }
        set { range = value; }
    }

    public string SkillType
    {
        get { return skillType; }
        set { skillType = value; }
    }

    public int UnitCastingStun
    {
        get { return unitCastingStun; }
        set { unitCastingStun = value; }
    }

    public int Cooldown
    {
        get { return cooldown; }
        set { cooldown = value; }
    }

    public int CurrentCooldown
    {
        get { return currentCooldown; }
        set { currentCooldown = value; }
    }

    public string CastingType
    {
        get { return castingType; }
        set { castingType = value; }
    }
}
