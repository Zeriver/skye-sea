﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EffectPopUp : MonoBehaviour
{
    public RawImage effectImage;
    public Text effectText;
    private Animator animator;
    private AudioSource audioSource;

    void OnEnable()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        Destroy(gameObject, clipInfo[0].clip.length);
    }


    public void setEffectInfo(string imageName, string text)
    {
        effectImage.texture = Resources.Load("Effect_Images/" + imageName) as Texture2D;
        audioSource.PlayOneShot(Resources.Load<AudioClip>("Prefabs/Sounds/" + imageName));
        effectText.text = text;
    }
}
