﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BeltItem : MonoBehaviour
{

    public RawImage image;
    public Text skillName;
    public Text description;

    private HealingItem healingItem;
    private PlayerUnitController unit;
    private bool isFlashingElement;
    private RawImage flashingElement;

    private 

    void Update()
    {
        
    }

    public void setItemInfo(HealingItem item, PlayerUnitController unit)
    {
        healingItem = item;
        skillName.text = item.name;
        description.text = item.description;
        this.unit = unit;
    }

    public void useItem()
    {
        unit.healingItemMode(healingItem);
    }
}
