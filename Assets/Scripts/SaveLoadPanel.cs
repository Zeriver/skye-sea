﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class SaveLoadPanel : MonoBehaviour
{


    public GameObject saveNewGameWindow;

    private SaveRecord currentlySelectedSaveSlot;
    private GameObject createNewTemp;

    UnityEngine.Object _saveRecord;
    UnityEngine.Object _saveRecordEmpty;

    void Start()
    {
        _saveRecord = Resources.Load("Prefabs/SaveSlotRecord");
        _saveRecordEmpty = Resources.Load("Prefabs/SaveSlotRecordEmpty");
    }


    void Update()
    {

    }

    void OnEnable()
    {
        if (_saveRecord == null)
        {
            _saveRecord = Resources.Load("Prefabs/SaveSlotRecord");
            _saveRecordEmpty = Resources.Load("Prefabs/SaveSlotRecordEmpty");
        }

        foreach (string file in System.IO.Directory.GetFiles(Application.dataPath + "/Saves/"))
        {
            string[] operators = { ".xml" };
            if (operators.Any(x => file.EndsWith(x)))
            {
                Debug.Log("Found Save File: " + file);
                GameObject saveRecord = (GameObject)GameObject.Instantiate(_saveRecord);
                saveRecord.GetComponent<SaveRecord>().setSaveRecordData(file);
                saveRecord.transform.SetParent(transform.GetChild(0));
            }
        }

        if (!SceneManager.GetActiveScene().name.Equals("MainMenu"))
        {
            createNewTemp = (GameObject)GameObject.Instantiate(_saveRecordEmpty);
            createNewTemp.transform.SetParent(transform.GetChild(0));
        }
    }

    public void showCreateNewSave()
    {
        saveNewGameWindow.SetActive(true);
        saveNewGameWindow.transform.GetChild(0).GetComponent<InputField>().text = "";
        saveNewGameWindow.transform.GetChild(0).GetComponent<InputField>().Select();
        saveNewGameWindow.transform.GetChild(0).GetComponent<InputField>().ActivateInputField();
    }

    public void hideCreateNewSave()
    {
        saveNewGameWindow.SetActive(false);
    }

    public void setSaveSlot(SaveRecord saveRecord)
    {
        currentlySelectedSaveSlot = saveRecord;
    }

    public void hideSavePanelView()
    {
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            Destroy(transform.GetChild(0).GetChild(i).gameObject);
        }
        gameObject.SetActive(false);
    }

    public void loadSelectedRecord()
    {
        if (currentlySelectedSaveSlot)
        {
            GameManager.instance.isGameLoaded = true;
            GameManager.instance.loadSavedInfo(currentlySelectedSaveSlot.getSavePath());
        }
    }

    public void saveSelectedRecord()
    {
        if (currentlySelectedSaveSlot)
        {
            GameManager.instance.saveGame(currentlySelectedSaveSlot.getSavePath());
        }
    }

    public void saveNewGame()
    {
        hideCreateNewSave();
        string saveName = saveNewGameWindow.transform.GetChild(0).GetComponentInChildren<Text>().text;
        string savePath = Application.dataPath + "/Saves/" + saveName + ".xml";
        Debug.Log(savePath);
        GameManager.instance.saveGame(savePath);

        GameObject saveRecord = (GameObject)GameObject.Instantiate(_saveRecord);
        saveRecord.GetComponent<SaveRecord>().setSaveRecordData(savePath);
        saveRecord.transform.SetParent(transform.GetChild(0));

        createNewTemp.transform.SetParent(null);
        createNewTemp.transform.SetParent(transform.GetChild(0));
    }
}
