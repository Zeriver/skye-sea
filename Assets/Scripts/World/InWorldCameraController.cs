﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InWorldCameraController : MonoBehaviour
{

    private GameObject cameraStartingPosition;
    private GameObject cameraPortingPosition;
    private GameObject cameraIslandPosition;
    private GameObject currentTarget;
    private bool isGameLoaded;

    public float maxVel = 15;
    public float startVel = 9;
    public float stopVel = 0.3f;
    public float accDistance = 20;
    public float factor = 0.25f; // max inclination
    public float turnSpeed = 0.8f;
    Vector3 lastPos;
    bool flying = false;

    void Start()
    {

    }

    void Update()
    {
        Vector3 dir = transform.position - lastPos;
        lastPos = transform.position;
        float dist = dir.magnitude;
        if (dist > 0.001f && currentTarget != null)
        {
            dir /= dist;
            float vel = dist / Time.deltaTime;
            Quaternion bankRot = Quaternion.LookRotation((dir * 2f) + factor * Vector3.down * vel / maxVel);
            //transform.rotation = Quaternion.Slerp(transform.rotation, bankRot, turnSpeed * Time.deltaTime);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, currentTarget.transform.rotation, 3 * Time.deltaTime);
        }
    }

    IEnumerator FlyTo(Vector3 targetPos, Quaternion targetRot)
    {
        flying = true;
        Vector3 startPos = transform.position;
        Vector3 dir = targetPos - startPos;
        float distTotal = dir.magnitude;
        dir /= distTotal;
        float accDist = Mathf.Min(accDistance, distTotal / 2);
        do
        {
            float dist1 = Vector3.Distance(transform.position, startPos);
            float dist2 = distTotal - dist1;
            float speed = maxVel;
            if (dist1 < accDist)
            {
                speed = Mathf.Lerp(startVel, maxVel, dist1 / accDist);
            }
            else
            if (dist2 < accDist)
            {
                speed = Mathf.Lerp(stopVel, maxVel, dist2 / accDist);
            }

            if (currentTarget == cameraIslandPosition)
            {
                speed *= 2.5f;
            }

            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, speed * Time.deltaTime);
            yield return 0;
        } while (transform.position != targetPos);
        flying = false;
    }

    public void setValuesOnLoadGame()
    {
        isGameLoaded = true;
        cameraStartingPosition = GameObject.Find("CameraStartingPosition");
        cameraPortingPosition = GameObject.Find("CameraPortingPosition");
        cameraIslandPosition = GameObject.Find("CameraIslandPosition");
        transform.position = cameraIslandPosition.transform.position;
        transform.rotation = cameraIslandPosition.transform.rotation;
    }

    public void startPortingMove()
    {
        cameraStartingPosition = GameObject.Find("CameraStartingPosition");
        cameraPortingPosition = GameObject.Find("CameraPortingPosition");
        transform.position = cameraStartingPosition.transform.position;
        transform.rotation = cameraStartingPosition.transform.rotation;
        StartCoroutine(FlyTo(cameraPortingPosition.transform.position, cameraPortingPosition.transform.rotation));
        currentTarget = cameraPortingPosition;
    }

    public void setFlyLeaveValues()
    {
        StartCoroutine(FlyTo(cameraStartingPosition.transform.position, cameraStartingPosition.transform.rotation));
        currentTarget = cameraStartingPosition;
    }

    public void setIslandView()
    {
        cameraIslandPosition = GameObject.Find("CameraIslandPosition");
        StartCoroutine(FlyTo(cameraIslandPosition.transform.position, cameraIslandPosition.transform.rotation));
        currentTarget = cameraIslandPosition;
    }

}
