﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : Weapon
{

    public Spear() : base()
    {
        name = "Spear";
        description = "Mr Long Stab";
        damageType = "thrust";
        damage = 8;
        cooldown = 0;
        range = 2;
        isDiagonal = false;
        isFlankingBonus = true;
        isMelee = true;
        heroOwner = "";
        holdingSpot = "RightHandSlot";
        inactiveSpot = "BackSlot";
        price = 200;
    }

    public Spear(string owner) : base()
    {
        name = "Spear";
        description = "Mr Long Stab";
        damageType = "thrust";
        damage = 8;
        cooldown = 0;
        range = 2;
        isDiagonal = false;
        isFlankingBonus = true;
        isMelee = true;
        heroOwner = owner;
        holdingSpot = "RightHandSlot";
        inactiveSpot = "BackSlot";
        price = 200;
    }

}
