﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectPopUpController : MonoBehaviour
{
    private static EffectPopUp popUp;
    private static GameObject canvas;

    public static void Initialize()
    {
        canvas = GameObject.Find("Effects_Canvas");
        if (!popUp)
        {
            popUp = Resources.Load<EffectPopUp>("Prefabs/EffectPopUpContainer");
        }
    }

    public static void createPopUp(string effectName, string additionalText, Transform location)
    {
        EffectPopUp EffectPopUp = Instantiate(popUp);
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(location.position);
        EffectPopUp.transform.SetParent(canvas.transform, false);
        EffectPopUp.transform.position = screenPosition + new Vector3(0.0f, 80.0f, 0.0f);
        EffectPopUp.setEffectInfo(effectName, additionalText);
    }
}
