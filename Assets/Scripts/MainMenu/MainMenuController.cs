﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Linq;

public class MainMenuController : MonoBehaviour
{

    public GameObject loadGamePanel;


    UnityEngine.Object _saveRecord;

    void Start()
    {
        _saveRecord = Resources.Load("Prefabs/SaveSlotRecord");
    }

    void Update()
    {

    }

    public void startNewGame()
    {
        World freshWorld = new World();
        freshWorld.setWorldForNewGame();
        GameManager.instance.setWorld(freshWorld);
        SceneManager.LoadScene("WorldViewBase");
    }

    public void startTutorialBattle()
    {
        World freshWorld = new World();
        freshWorld.setWorldForNewGame();
        GameManager.instance.setWorld(freshWorld);
        List<Unit> selectedUnits = new List<Unit>();
        selectedUnits.Add(freshWorld.getHeroes()[1]);
        selectedUnits.Add(freshWorld.getHeroes()[0]);
        selectedUnits.Add(freshWorld.getHeroes()[2]);
        GameManager.instance.loadBattleGrounds("SampleScene", selectedUnits);
    }

    public void loadSavedGame()
    {
        loadGamePanel.SetActive(!loadGamePanel.activeSelf);

        foreach (string file in System.IO.Directory.GetFiles(Application.dataPath + "/Saves/"))
        {
            string[] operators = { ".xml" };
            if (operators.Any(x => file.EndsWith(x)))
            {
                Debug.Log("Found Save File: " + file);
                GameObject saveRecord = (GameObject)GameObject.Instantiate(_saveRecord);
                saveRecord.GetComponent<SaveRecord>().setSaveRecordData(file);
                saveRecord.transform.parent = loadGamePanel.transform;
            }
        }
    }

}
