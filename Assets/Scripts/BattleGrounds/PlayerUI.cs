﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerUI : MonoBehaviour
{
    public Text turn = null;
    public Text unitName;
    public Text health;
    public RawImage unitPortrait;
    public RawImage itemImage;
    public GameObject skillsPanel;
    public GameObject beltPanel;
    public GameObject skillButton;
    public GameObject beltButton;
    public List <GameObject> weaponButtons;
    public GameObject endTurnButton;

    private Animator _animator;
    private bool isFlashingElement;
    private RawImage flashingElement;
    private Color originalColor;
    private string highlightedNestedElement;

    UnityEngine.Object _skillItem;
    UnityEngine.Object _beltItem;

    public bool IsOpen
    {
        get { return _animator.GetBool("IsOpen"); }
        set { _animator.SetBool("IsOpen", value); }
    }

    public void Awake()
    {
        _animator = GetComponent<Animator>();
        IsOpen = true;

        _skillItem = Resources.Load("Prefabs/SkillItem");
        _beltItem = Resources.Load("Prefabs/BeltItem");

        //var rect = GetComponent<RectTransform>();
        //rect.offsetMax = rect.offsetMin = new Vector2(0, 0);
    }

    public void Update()
    {
        if (isFlashingElement && flashingElement != null)
        {
            Color lerpedColor = Color.Lerp(Color.yellow, Color.magenta, Mathf.PingPong(Time.time, 1));
            flashingElement.color = lerpedColor;
        }
    }

    public void setHealth(int value)
    {
        health.text = value.ToString() + " HP";
    }

    public void setName(string name)
    {
        unitName.text = name;
    }

    public void setUnitInfo(PlayerUnitController unit)
    {
        this.health.text = unit.health.ToString() + " HP";
        unitName.text = unit.getUnitName();
        unitPortrait.texture = Resources.Load("Character_Portraits/" + unit.getUnitName() + "_portrait") as Texture2D;
        itemImage.texture = Resources.Load("Item_Images/" + unit.currentItem.name + "_TEMP") as Texture2D;
    }

    public void setCurrentItem(string name)
    {
        itemImage.texture = Resources.Load("Item_Images/" + name + "_TEMP") as Texture2D;
    }

    public void updateTurn(int turnNumber)
    {
        turn.text = "Turn: " + turnNumber;
    }

    public void changeSkillPanel(PlayerUnitController unit)
    {
        skillsPanel.SetActive(!skillsPanel.activeSelf);
        if (skillsPanel.activeSelf)
        {
            for (int i = 0; i < unit.skills.Count; i++)
            {
                GameObject itemPrefab = (GameObject)GameObject.Instantiate(_skillItem);
                itemPrefab.transform.SetParent(skillsPanel.transform);
                itemPrefab.GetComponent<SkillItem>().setSkillInfo(unit.skills[i], unit);
                if (isFlashingElement && unit.skills[i].Name.Equals(highlightedNestedElement))
                {
                    skillButton.GetComponent<RawImage>().color = originalColor;
                    flashingElement = null;
                    isFlashingElement = false;
                    itemPrefab.GetComponent<SkillItem>().startFlashing();
                }
            }
        }
        else
        {
            for (int i = 0; i < skillsPanel.transform.childCount; i++)
            {
                Destroy(skillsPanel.transform.GetChild(i).gameObject);
            }
        }
    }

    public void changeBeltPanel(PlayerUnitController unit)
    {
        beltPanel.SetActive(!beltPanel.activeSelf);
        if (beltPanel.activeSelf)
        {
            for (int i = 0; i < unit.healingItems.Count; i++)
            {
                GameObject itemPrefab = (GameObject)GameObject.Instantiate(_beltItem);
                itemPrefab.transform.SetParent(beltPanel.transform);
                itemPrefab.GetComponent<BeltItem>().setItemInfo(unit.healingItems[i], unit);
                if (isFlashingElement && unit.healingItems[i].name.Equals(highlightedNestedElement))
                {
                    beltButton.GetComponent<RawImage>().color = originalColor;
                    flashingElement = null;
                    isFlashingElement = false;
                    //itemPrefab.GetComponent<SkillItem>().startFlashing();
                }
            }
        }
        else
        {
            for (int i = 0; i < beltPanel.transform.childCount; i++)
            {
                Destroy(beltPanel.transform.GetChild(i).gameObject);
            }
        }
    }

    public void skillFlashing(string skillToHighlight)
    {
        isFlashingElement = true;
        flashingElement = skillButton.GetComponent<RawImage>();
        originalColor = skillButton.GetComponent<RawImage>().color;
        highlightedNestedElement = skillToHighlight;
    }

    public void weaponFlashing(PlayerUnitController unit, string weaponName)
    {
        isFlashingElement = true;
        if (weaponName.Equals(((Weapon)unit.currentItem).getName()))
        {
            flashingElement = weaponButtons[0].GetComponent<RawImage>();
            originalColor = weaponButtons[0].GetComponent<RawImage>().color;
            setButtonsInteractable(false, "Weapon");
        }
        else
        {
            flashingElement = weaponButtons[1].GetComponent<RawImage>();
            originalColor = weaponButtons[1].GetComponent<RawImage>().color;
            setButtonsInteractable(false, "ToggleWeapon");
        }
    }

    public void weaponAction()
    {
        if (isFlashingElement && flashingElement.Equals(weaponButtons[0].GetComponent<RawImage>()))
        {
            weaponButtons[0].GetComponent<RawImage>().color = originalColor;
            flashingElement = null;
            isFlashingElement = false;
        }
        else if (isFlashingElement && flashingElement.Equals(weaponButtons[1].GetComponent<RawImage>()))
        {
            setButtonsInteractable(true, "");
            weaponButtons[1].GetComponent<RawImage>().color = originalColor;
            flashingElement = weaponButtons[0].GetComponent<RawImage>();
            originalColor = weaponButtons[0].GetComponent<RawImage>().color;
            setButtonsInteractable(false, "Weapon");
        }
    }

    public void setButtonsInteractable(bool value, string exception)
    {
        if (!endTurnButton.name.Equals(exception))
        {
            endTurnButton.GetComponent<Button>().interactable = value;
        }
        if (!skillButton.name.Equals(exception))
        {
            skillButton.GetComponent<Button>().interactable = value;
        }
        if (!beltButton.name.Equals(exception))
        {
            beltButton.GetComponent<Button>().interactable = value;
        }
        for (int i = 0; i < weaponButtons.Count; i++)
        {
            if (!weaponButtons[i].name.Equals(exception))
            {
                weaponButtons[i].GetComponent<Button>().interactable = value;
            }
        }
    }

}
