﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeatherCap : Item
{

    public LeatherCap() : base()
    {
        name = "Leather Cap";
        description = "One Hat A Day Keeps Arrows Away!";
        heroOwner = ""; //TODO temporary + more infor about armour etc
        holdingSpot = "Head";
        price = 120;
    }

    public LeatherCap(string owner) : base()
    {
        name = "Leather Cap";
        description = "One Hat A Day Keeps Arrows Away!";
        heroOwner = owner; //TODO temporary + more infor about armour etc
        holdingSpot = "Head";
        price = 120;
    }
}
