﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SkillsPanel : MonoBehaviour
{
    public Text skillDescription;
    public Text skillPoints;
    public List<GameObject> unitSkillPanels;

    private GameObject currentUnitSkillPanel;
    private List<GameObject> skillButtons;

    void Start()
    {

    }

    void Update()
    {

    }

    public void populateUnitSkills(Unit unit)
    {
        for (int i = 0; i < unitSkillPanels.Count; i++)
        {
            if (unitSkillPanels[i].name.Equals(unit.getName()))
            {
                currentUnitSkillPanel = unitSkillPanels[i];
                unitSkillPanels[i].SetActive(true);
            }
            else
            {
                unitSkillPanels[i].SetActive(false);
            }
        }

        skillButtons = new List<GameObject>();
        for (int i = 0; i < currentUnitSkillPanel.transform.childCount; i++)
        {
            skillButtons.Add(currentUnitSkillPanel.transform.GetChild(i).gameObject);
            skillButtons[i].GetComponent<SkillButton>().setButtonSkill(this, transform.parent.parent.GetComponent<EquipmentController>().getCurrentUnit());
        }

        for (int i = 0; i < unit.getSkills().Count; i++)
        {
            for (int j = 0; j < skillButtons.Count; j++)
            {
                if (skillButtons[j].name.Equals(unit.getSkills()[i].Name))
                {
                    skillButtons[j].GetComponent<SkillButton>().setButtonSkillActive();
                }
            }
        }

        skillPoints.text = "Skill Points: " + unit.SkillPoints.ToString();
    }

    public void addSkill(string skillName)
    {
        transform.parent.parent.GetComponent<EquipmentController>().getCurrentUnit().addNewSkill(createSkillForUnit(skillName));
        skillPoints.text = "Skill Points: " + transform.parent.parent.GetComponent<EquipmentController>().getCurrentUnit().SkillPoints.ToString();
    }

    public void showDescription(string skillName)
    {
        Skill skill = createSkillForUnit(skillName);
        if (skill != null)
        {
            skillDescription.text = skill.Description;
        }
        else
        {
            skillDescription.text = "";
        }
        
    }

    public Skill createSkillForUnit(string skillName)
    {
        if (skillName.Equals("Force Push"))
        {
            return new Push();
        }
        return null;
    }
}
