﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindmillBigWing : MonoBehaviour
{

    private float speed;
    private float newSpeed;

    void Start()
    {
        speed = 35;
        newSpeed = speed;
        StartCoroutine(speedChange());
    }

    void Update()
    {
        transform.localRotation = Quaternion.Euler(new Vector3(-89.98f, -Mathf.PingPong(Time.time * speed, 80), 106.658f));
        if (newSpeed > speed)
        {
            speed += 0.005f;
        }
        if (newSpeed < speed)
        {
            speed -= 0.005f;
        }
    }

    IEnumerator speedChange()
    {
        yield return new WaitForSeconds(2);
        newSpeed = Random.Range(10, 15);
        StartCoroutine(speedChange());
    }

}
