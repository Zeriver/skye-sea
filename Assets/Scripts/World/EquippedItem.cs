﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EquippedItem : MonoBehaviour
{
    private Item assignedItem;
    private RawImage itemImage;

    private void Start()
    {
        itemImage = GetComponentInChildren<RawImage>();
    }

    public void setEquippedItem(Item item)
    {
        assignedItem = item;
        if (itemImage == null)
        {
            itemImage = GetComponentInChildren<RawImage>();
        }
        itemImage.texture = Resources.Load<Texture2D>("Prefabs/Textures/" + item.name + "_item");
    }

    public Item getAssignedItem()
    {
        return assignedItem;
    }

    public void resetEquipment()
    {
        assignedItem = null;
        itemImage.texture = Resources.Load<Texture2D>("Prefabs/Textures/itemSlot");
    }

    public void unequipItem()
    {
        transform.parent.parent.parent.GetComponent<EquipmentController>().unequipItem(assignedItem);
        assignedItem = null;
        itemImage.texture = Resources.Load<Texture2D>("Prefabs/Textures/itemSlot");
    }

}
