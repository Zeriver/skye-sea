﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SailsController : MonoBehaviour
{

    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {

    }


    public void stopSails()
    {
        Debug.Log("STOPPING");
        anim.SetBool("isStopping", true);
    }

    public void startSails()
    {
        anim.SetBool("isStopping", false);
    }

}
