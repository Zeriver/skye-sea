﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;

public class PlayerUnitController : UnitController
{
    public List<GameObject> itemSlots;

    public bool isSelected;
    public bool isActionMode;
    public Tile currentScriptedTileTarget;
    public List<GameObject> skillHighlight;
    public List<GameObject> giveHighlights;
    public List<GameObject> pushHighlights;
    private List<Tile> validTiles;
    private Tile skillTile;
    private Vector3 actionMouseHighlight;
    private MouseHighlight _mouseHiglight;

    private bool isSkillCharged;

    UnityEngine.Object _chargingPrefab;

    void Start()
    {

    }

    public void createPlayerUnit(int x, int y, string facingDirection, Unit unit)
    {
        BattleGroundObject = GameObject.Find("BattleGrounds");
        _mouseHiglight = BattleGroundObject.GetComponent("MouseHighlight") as MouseHighlight;
        _tileMapBuilder = BattleGroundObject.GetComponent("TileMapBuilder") as TileMapBuilder;
        _battleGroundController = BattleGroundObject.GetComponent("BattleGroundController") as BattleGroundController;
        //InventoryCanvas = GameObject.Find("EquipmentCanvas").GetComponent<Canvas>();
        anim = GetComponent<Animator>();

        _chargingPrefab = Resources.Load("Prefabs/SpellCharging");

        audioSource = GetComponent<AudioSource>();
        isSelected = false;
        showMoves = false;
        isActionMode = false;
        isActionUsed = false;
        moving = false;
        coordinates = new Vector3(x, 0.0f, y);
        transform.position = new Vector3(coordinates.x + 0.5f, coordinates.y, -coordinates.z + 0.5f);
        targetRotation = transform.rotation;
        validTiles = new List<Tile>();
        movementHighlights = new List<GameObject>();
        weapons = new List<Weapon>();
        healingItems = new List<HealingItem>();
        weaponHighlights = new List<GameObject>();
        skillHighlight = new List<GameObject>();
        giveHighlights = new List<GameObject>();
        pushHighlights = new List<GameObject>();
        weaponAreaEffectHighlights = new List<GameObject>();
        skillAreaEffectHighlights = new List<GameObject>();
        positionQueue = new List<Vector3>();
        setPositions();
        targets = new List<UnitController>();
        obstaclesToAttack = new List<Obstacle>();
        TileMap.setTileNotWalkable(x, y);
        healthEffects = new List<int>();
        currentEffect = "none";
        moveSpeed = 2.2f;

        associatedUnit = unit;
        unitName = unit.getName();
        className = unit.getClassName();

        maxHealth = unit.MaxHealth;
        health = unit.CurrentHealth;
        maxMovement = unit.MaxMovement;
        movesLeft = maxMovement;
        totalExperience = unit.TotalExperience;
        fireResistance = unit.FireResistance;
        freezeResistance = unit.FreezeResistance;
        poisonResistance = unit.PoisonResistance;
        defendBonus = unit.DefendBonus;

        switch (facingDirection)
        {
            case "up":
                transform.rotation = Quaternion.Euler(new Vector3(0.0f, 90.0f, 0.0f));
                break;
            case "right":
                transform.rotation = Quaternion.Euler(new Vector3(0.0f, 180.0f, 0.0f));
                break;
            case "down":
                transform.rotation = Quaternion.Euler(new Vector3(0.0f, -90.0f, 0.0f));
                break;
            case "left":
                transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 0.0f));
                break;
        }
        this.facingDirection = facingDirection;

        //EQ

        weapons.Add(new OneHandedAxe(unitName));
        if (unitName.Equals("Blitz"))
        {
            weapons.Add(new Bow(unitName, 5));
            healingItems.Add(new SmallHealingPotion(1));
        }
        else
        {
            weapons.Add(new Spear(unitName));
        }
        currentItem = weapons[0];


        weapons[0].weaponModel = (GameObject)Instantiate(Resources.Load("Prefabs/" + weapons[0].getName()), getItemSlotObject(weapons[0].holdingSpot).transform.position, getItemSlotObject(weapons[0].holdingSpot).transform.rotation);
        weapons[0].weaponModel.transform.SetParent(getItemSlotObject(weapons[0].holdingSpot).transform);
        weapons[1].weaponModel = (GameObject)Instantiate(Resources.Load("Prefabs/" + weapons[1].getName()), getItemSlotObject(weapons[1].inactiveSpot).transform.position, getItemSlotObject(weapons[1].inactiveSpot).transform.rotation);
        weapons[1].weaponModel.transform.SetParent(getItemSlotObject(weapons[1].inactiveSpot).transform);

        for (int i = 0; i < weapons.Count; i++)
        {
            if (weapons[i].additionalItem != null && !weapons[i].additionalItem.Equals(""))
            {
                GameObject additionalItem = (GameObject)Instantiate(Resources.Load("Prefabs/" + weapons[i].additionalItem), getItemSlotObject(weapons[1].additionalItemSpot).transform.position, getItemSlotObject(weapons[1].additionalItemSpot).transform.rotation);
                additionalItem.transform.SetParent(getItemSlotObject(weapons[1].additionalItemSpot).transform);
            }
        }

        for (int i = 0; i < healingItems.Count; i++)
        {
            healingItems[i].itemModel = (GameObject)Instantiate(Resources.Load("Prefabs/" + healingItems[i].name), getItemSlotObject(healingItems[i].inactiveSpot).transform.position, getItemSlotObject(healingItems[i].inactiveSpot).transform.rotation);
            healingItems[i].itemModel.transform.SetParent(getItemSlotObject(healingItems[i].inactiveSpot).transform);
        }

        currentSkill = null;

        //TEMPORARY, REMOVE LATER
        skills = new List<Skill>();
        skills.Add(new ForceField());

    }

    void Update()
    {
        if (enteringPosition)
        {
            if (positionQueue.Count > 0 && !moving)
            {
                setNextStep(new Vector3[]  {            //Temporary fix until proper units models will be in game TODO
                     /*new Vector3(0.0f, 180.0f, 0.0f),
                     new Vector3(0.0f, 0.0f, 0.0f),
                     new Vector3(0.0f, 90.0f, 0.0f),
                     new Vector3(0.0f, -90.0f, 0.0f)*/
                     new Vector3(0.0f, 90.0f, 0.0f),
                     new Vector3(0.0f, -90.0f, 0.0f),
                     new Vector3(0.0f, 0.0f, 0.0f),
                     new Vector3(0.0f, 180.0f, 0.0f)

                 });
                showMoves = true;
            }
            if (moving)
            {
                anim.SetTrigger("walkTrigger");
                anim.SetFloat("walkSelect", 0.4f);
                moveToNextStep(0);
                if (positionQueue.Count == 0)
                {
                    anim.SetFloat("walkSelect", 0f);
                }
            }
            if (turningToTarget)
            {
                turnToEnemy(currentSkill);
            }

            if (positionQueue.Count == 0 && !turningToTarget)
            {
                enteringPosition = false;
                moving = false;
            }
            else
            {
                return;
            }
        }

        if (isSelected)
        {
            if (showMoves && positionQueue.Count == 0)
            {
                showAllowedMovements();
            }
            if (positionQueue.Count > 0 && !moving)
            {
                setNextStep(new Vector3[]  {            //Temporary fix until proper units models will be in game TODO
                     /*new Vector3(0.0f, 180.0f, 0.0f),
                     new Vector3(0.0f, 0.0f, 0.0f),
                     new Vector3(0.0f, 90.0f, 0.0f),
                     new Vector3(0.0f, -90.0f, 0.0f)*/
                     new Vector3(0.0f, 90.0f, 0.0f),
                     new Vector3(0.0f, -90.0f, 0.0f),
                     new Vector3(0.0f, 0.0f, 0.0f),
                     new Vector3(0.0f, 180.0f, 0.0f)

                 });
                showMoves = true;
            }
            if (moving)
            {
                anim.SetTrigger("walkTrigger");
                anim.SetFloat("walkSelect", 0.4f);
                moveToNextStep(0);
                if (positionQueue.Count == 0)
                {
                    anim.SetFloat("walkSelect", 0f);
                    calculateDefendBonus(true);
                    _battleGroundController.setCameraMovementBlock(false);
                }
            }
            if (turningToTarget)
            {
                turnToEnemy(currentSkill);
            }
            if (useSkill)
            {
                if (skillTile != null)
                {
                    _battleGroundController.blockUIActionsForUnit(currentSkill.Name);
                    if (currentSkill.UnitCastingStun > 0 && currentSkill.CastingType.Equals("Charging") && !isSkillCharged)
                    {
                        //TODO start anim
                        skillChargingCounter = currentSkill.UnitCastingStun;
                        movesLeft = 0;
                        showMoves = true;
                        chargingEffect = (GameObject)GameObject.Instantiate(_chargingPrefab, transform.position + new Vector3(0.0f, 0.2f, 0.0f), Quaternion.Euler(new Vector3(-90.0f, 0.1f, 0.0f)));
                    }
                    else
                    {
                        currentSkill.useSkill(skillTile);
                        currentSkill = null;
                        skillTile = null;
                        showMoves = true;
                        if (chargingEffect != null)
                        {
                            chargingEffect.GetComponent<ParticleSystem>().Stop();
                            Destroy(chargingEffect, 5.0f);
                        }
                        isSkillCharged = false; //TODO Remove later when checking the animation will be available
                    }
                }
                isActionUsed = true;
                destroySkillHiglights();
                for (int i = 0; i < skillAreaEffectHighlights.Count; i++)
                {
                    Destroy(skillAreaEffectHighlights[i]);
                }
                _battleGroundController.setCameraMovementBlock(false);
                useSkill = false;
            }
            if (attack)
            {
                if (currentItem is Weapon)
                {
                    _battleGroundController.playerUnitActionsBlocked = true;
                    if (((Weapon)currentItem).useWeapon())
                    {
                        anim.SetTrigger("swordStrikeTrigger");
                        anim.SetInteger("swordStrikeSelect", 1);
                        StartCoroutine(idleAfterAttack());
                        for (int i = 0; i < targets.Count; i++)
                        {
                            targets[i].getAttacked(((Weapon)currentItem), this, getBonusDamageFromWeaponSkill());
                        }
                        for (int i = 0; i < obstaclesToAttack.Count; i++)
                        {
                            obstaclesToAttack[i].getDamaged(((Weapon)currentItem), getBonusDamageFromWeaponSkill());
                        }
                        isActionUsed = true;
                        switchActionMode();
                    }
                    targets.Clear();
                    obstaclesToAttack.Clear();
                    attack = false;
                }
                else if (currentItem is HealingItem)
                {
                    if (((HealingItem)currentItem).use())
                    {
                        for (int i = 0; i < targets.Count; i++)
                        {
                            targets[i].getHealed(((HealingItem)currentItem));
                        }
                        switchActionMode();
                        isActionUsed = true;
                        if (((HealingItem)currentItem).isLastStackable())
                        {
                            healingItems.Remove((HealingItem)currentItem);
                            currentItem = null;
                        }
                    }
                    targets.Clear();
                    switchWeapon();  //TODO add anims and delayed switch
                    attack = false;
                }
            }
            if (Input.GetMouseButtonDown(0) && positionQueue.Count == 0 && !turningToTarget 
                && !_battleGroundController.playerUnitActionsBlocked)                               //LEFT CLICK
            {
                Vector3 mousePosition = Input.mousePosition;
                if (mousePosition.y < Screen.height * 0.1681271) //divided by player ui anchor
                {
                    return;
                }
                Tile clickedTile = TileMap.getTile((int)_mouseHiglight.getHighlightSelection().position.x, (int)_mouseHiglight.getHighlightSelection().position.z);

                if (currentScriptedTileTarget != null)
                {
                    if (!clickedTile.Equals(currentScriptedTileTarget))
                    {
                        return;
                    }
                    else
                    {
                        currentScriptedTileTarget = null;
                        _battleGroundController.performPlayerAction();
                    }
                }

                if (!isActionMode && skillHighlight.Count == 0 && giveHighlights.Count == 0 && pushHighlights.Count == 0)
                {
                    if (validTiles.Contains(clickedTile))
                    {
                        TileMap.setTileWalkable((int)coordinates.x, (int)coordinates.z);
                        destroyMovementHiglights();
                        List<Tile> path = TilePathFinder.FindPath(TileMap.getTile((int)coordinates.x, (int)coordinates.z), clickedTile, false);
                        for (int i = 0; i < path.Count; i++)
                        {
                            positionQueue.Add(new Vector3(path[i].PosX, coordinates.y, path[i].PosY));
                        }
                        int pathCost = 0;
                        for (int i = 0; i < path.Count; i++)
                        {
                            pathCost += path[i].MoveCost;
                        }
                        movesLeft -= pathCost;
                        _battleGroundController.setCameraMovementBlock(true);
                    }
                }
                else if (isActionMode && validTiles != null)
                {
                    if (validTiles.Contains(clickedTile))
                    {
                        if (currentItem is Weapon)
                        {
                            List<Tile> weaponEffect = ((Weapon)currentItem).getAreaEffect(Math.Abs((int)coordinates.x), Math.Abs((int)coordinates.z), clickedTile.PosX, clickedTile.PosY);
                            List<UnitController> unitsInArea = new List<UnitController>();
                            unitsInArea.AddRange(_battleGroundController.enemyUnits.Cast<UnitController>());
                            unitsInArea.AddRange(_battleGroundController.playerUnits.Cast<UnitController>());
                            for (int j = 0; j < weaponEffect.Count; j++)
                            {
                                for (int i = 0; i < unitsInArea.Count; i++)
                                {
                                    for (int x = 0; x < unitsInArea[i].positions.Count; x++)
                                    {
                                        if (TileMap.getTile((int)unitsInArea[i].positions[x].x, (int)unitsInArea[i].positions[x].z - 1).Equals(weaponEffect[j]) && !targets.Contains(unitsInArea[i]))
                                        {
                                            targets.Add(unitsInArea[i]);
                                            break;
                                        }
                                    }
                                }
                                if (TileMap.getObstacleAt(weaponEffect[j].PosX, weaponEffect[j].PosY) != null)
                                {
                                    obstaclesToAttack.Add(TileMap.getObstacleAt(weaponEffect[j].PosX, weaponEffect[j].PosY));
                                }
                            }
                        }
                        else if (currentItem is HealingItem)
                        {
                            List<UnitController> unitsInArea = new List<UnitController>();
                            unitsInArea.AddRange(_battleGroundController.playerUnits.Cast<UnitController>());
                            for (int i = 0; i < unitsInArea.Count; i++)
                            {
                                for (int j = 0; j < unitsInArea[i].positions.Count; j++)
                                {
                                    if (TileMap.getTile((int)unitsInArea[i].positions[j].x, (int)unitsInArea[i].positions[j].z - 1).Equals(clickedTile))
                                    {
                                        targets.Add(unitsInArea[i]);
                                        break;
                                    }
                                }
                            }
                        }
                        if (clickedTile.PosX > coordinates.x)
                            targetRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
                        else if (clickedTile.PosX < coordinates.x)
                            targetRotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
                        else if (clickedTile.PosY < coordinates.z)
                            targetRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                        else if (clickedTile.PosY > coordinates.z)
                            targetRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
                        turningToTarget = true;

                        _battleGroundController.setCameraMovementBlock(true);
                        _battleGroundController.blockUIActionsForUnit(currentItem.name);
                    }
                }
                else if (skillHighlight.Count != 0)
                {
                    if (validTiles.Contains(clickedTile))
                    {
                        skillTile = clickedTile;

                        if (clickedTile.PosX > coordinates.x)
                            targetRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
                        else if (clickedTile.PosX < coordinates.x)
                            targetRotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
                        else if (clickedTile.PosY < coordinates.z)
                            targetRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                        else if (clickedTile.PosY > coordinates.z)
                            targetRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
                        turningToTarget = true;
                    }
                }
                else if (giveHighlights.Count != 0)
                {
                    for (int i = 0; i < _battleGroundController.playerUnits.Count; i++)
                    {
                        if (_battleGroundController.playerUnits[i].getUnitTile().Equals(clickedTile))
                        {
                            if (currentItem is Weapon)
                            {
                                _battleGroundController.playerUnits[i].weapons.Add((Weapon)currentItem);
                                weapons.Remove((Weapon)currentItem);
                                currentItem = null;
                            }
                            else if (currentItem is HealingItem)
                            {
                                if (currentItem.isStackable)
                                {
                                    for (int j = 0; j < _battleGroundController.playerUnits[i].healingItems.Count; j++)
                                    {
                                        if (_battleGroundController.playerUnits[i].healingItems[j].name.Equals(currentItem.name))
                                        {
                                            _battleGroundController.playerUnits[i].healingItems[j].addAmount(((HealingItem)currentItem).amount);
                                        }
                                    }
                                }
                                else
                                {
                                    _battleGroundController.playerUnits[i].healingItems.Add((HealingItem)currentItem);
                                }
                                healingItems.Remove((HealingItem)currentItem);
                                currentItem = null;
                            }
                            giveMode();
                        }
                    }
                }
                else if (pushHighlights.Count != 0)
                {
                    if (clickedTile.IsPushable)
                    {
                        Tile pushableTile;
                        if (coordinates.x > clickedTile.PosX)
                            pushableTile = TileMap.getTile(clickedTile.PosX - 1, clickedTile.PosY);
                        else if (coordinates.x < clickedTile.PosX)
                            pushableTile = TileMap.getTile(clickedTile.PosX + 1, clickedTile.PosY);
                        else if (coordinates.z > clickedTile.PosY)
                            pushableTile = TileMap.getTile(clickedTile.PosX, clickedTile.PosY - 1);
                        else
                            pushableTile = TileMap.getTile(clickedTile.PosX, clickedTile.PosY + 1);
                        if (pushableTile.IsWalkable)
                        {
                            pushableTile.IsWalkable = false;
                            pushableTile.IsPushable = true;
                            pushableTile.Id = 4;
                            pushableTile.IsUnitOnIt = clickedTile.IsUnitOnIt;
                            pushableTile.IsBlockingWeapons = clickedTile.IsBlockingWeapons;

                            TileMap.setTileWalkable((int)coordinates.x, (int)coordinates.z);
                            TileMap.resetObstacleTile(clickedTile);

                            TileMap.getObstacleAt(clickedTile.PosX, clickedTile.PosY).moveOneTile(pushableTile, moveSpeed);
                            List<Tile> path = TilePathFinder.FindPath(TileMap.getTile((int)coordinates.x, (int)coordinates.z), clickedTile, false);
                            for (int i = 0; i < path.Count; i++)
                            {
                                positionQueue.Add(new Vector3(path[i].PosX, coordinates.y, path[i].PosY));
                            }
                            movesLeft -= positionQueue.Count;
                            isActionUsed = true;
                            pushMode();
                        }
                    }
                }
            }
            if (Input.GetMouseButtonDown(1) && positionQueue.Count == 0 && !isActionUsed && _battleGroundController.playerTurn) //RIGHT CLICK
            {
                switchActionMode();
            }
            if ((isActionMode || (currentSkill != null && skillHighlight.Count > 0)) && validTiles != null) //HOVER
            {
                Tile hoverTile = TileMap.getTile((int)_mouseHiglight.getHighlightSelection().position.x, (int)_mouseHiglight.getHighlightSelection().position.z);
                if (validTiles.Contains(hoverTile) && (currentItem is Weapon || skillHighlight.Count > 0))
                {
                    actionMouseHighlight = new Vector3((int)_mouseHiglight.getHighlightSelection().position.x + 0.5f, 0.0f, (int)_mouseHiglight.getHighlightSelection().position.z + 0.5f);
                    
                    int mouseX = Math.Abs((int)actionMouseHighlight.x);
                    int mouseY = Math.Abs((int)actionMouseHighlight.z - 1);

                    if (skillHighlight.Count > 0)
                    {
                        for (int i = 0; i < skillAreaEffectHighlights.Count; i++)
                        {
                            Destroy(skillAreaEffectHighlights[i]);
                        }
                        List<Tile> skillAreaEffect = currentSkill.getAreaEffect(Math.Abs((int)coordinates.x), Math.Abs((int)coordinates.z), mouseX, mouseY);
                        highlightAreaEffect(skillAreaEffect, false);
                    }
                    else
                    {
                        for (int i = 0; i < weaponAreaEffectHighlights.Count; i++)
                        {
                            Destroy(weaponAreaEffectHighlights[i]);
                        }
                        List<Tile> weaponAreaEffect = ((Weapon)currentItem).getAreaEffect(Math.Abs((int)coordinates.x), Math.Abs((int)coordinates.z), mouseX, mouseY);
                        highlightAreaEffect(weaponAreaEffect, true);
                    }
                }
                else if (!(validTiles.Contains(hoverTile) && actionMouseHighlight != null))
                {
                    for (int i = 0; i < weaponAreaEffectHighlights.Count; i++)
                    {
                        Destroy(weaponAreaEffectHighlights[i]);
                    }
                    for (int i = 0; i < skillAreaEffectHighlights.Count; i++)
                    {
                        Destroy(skillAreaEffectHighlights[i]);
                    }
                }
            }
        }
    }

    public void showAllowedMovements()
    {
        destroyMovementHiglights();
        validTiles = TileHighlight.FindHighlight(TileMap.getTile((int)coordinates.x, (int)coordinates.z), movesLeft, false, false);
        for (int i = 0; i < validTiles.Count; i++)
        {
            int x = Mathf.FloorToInt(validTiles[i].PosX / _tileMapBuilder.tileSize);
            int z = Mathf.FloorToInt(validTiles[i].PosY * (-1) / _tileMapBuilder.tileSize);  //* -1 because battleGround generates on negative z TODO
            if (validTiles[i].Equals(currentScriptedTileTarget))
            {
                movementHighlights.Add(createPlane(x, z, new Color(1.0f, 0.0f, 0.0f, 0.3f)));
            }
            else
            {
                movementHighlights.Add(createPlane(x, z, new Color(0.5f, 0.85f, 0.0f, 0.3f)));
            }
        }
        showMoves = false;
    }

    private void highlightWeaponRange()
    {
        Color tileColor = new Color(1.0f, 0.0f, 0.05f, 0.3f);
        if (currentItem is Weapon)
        {
            validTiles = ((Weapon)currentItem).getWeaponHighlights((int)coordinates.x, (int)coordinates.z);
        }
        else if (currentItem is HealingItem)
        {
            validTiles = ((HealingItem)currentItem).getHealingHighlights((int)coordinates.x, (int)coordinates.z);
            tileColor = new Color(0.0f, 0.0f, 1.0f, 0.3f);
        }
        else
        {
            validTiles = null;
        }
        if (validTiles != null)
        {
            for (int i = 0; i < validTiles.Count; i++)
            {
                int x = Mathf.FloorToInt(validTiles[i].PosX / _tileMapBuilder.tileSize);
                int z = Mathf.FloorToInt(validTiles[i].PosY * (-1) / _tileMapBuilder.tileSize);  //* -1 because battleGround generates on negative z TODO
                weaponHighlights.Add(createPlane(x, z, tileColor));
            }
        }
    }

    private void highlightAreaEffect(List<Tile> areaEffect, bool isWeapon)
    {
        for (int i = 0; i < areaEffect.Count; i++)
        {
            int x = Mathf.FloorToInt(areaEffect[i].PosX / _tileMapBuilder.tileSize);
            int z = Mathf.FloorToInt(areaEffect[i].PosY * (-1) / _tileMapBuilder.tileSize);  //* -1 because battleGround generates on negative z TODO
            if (isWeapon)
            {
                weaponAreaEffectHighlights.Add(createPlane(x, z, new Color(8.0f, 8.0f, 0.0f, 0.3f)));
            }
            else
            {
                skillAreaEffectHighlights.Add(createPlane(x, z, new Color(8.0f, 8.0f, 0.0f, 0.3f)));
            }
        }
    }

    private void setActionMode(bool value)
    {
        isActionMode = !value;
        switchActionMode();
    }

    public void switchActionMode()
    {
        if (skillHighlight.Count > 0)
        {
            destroySkillHiglights();
        }
        if (giveHighlights.Count > 0)
        {
            destroyGiveHiglights();
        }
        if (pushHighlights.Count > 0)
        {
            destroyPushHiglights();
        }
        for (int i = 0; i < _battleGroundController.enemyUnits.Count; i++)
        {
            _battleGroundController.enemyUnits[i].destroyMovementHighlights();
        }

        isActionMode = !isActionMode;
        if (isActionMode && !isActionUsed)
        {
            destroyMovementHiglights();
            highlightWeaponRange();
        }
        else
        {
            DestroyWeaponHiglights();
            for (int i = 0; i < weaponAreaEffectHighlights.Count; i++)
            {
                Destroy(weaponAreaEffectHighlights[i]);
            }
            showMoves = true;
        }
    }

    public void DestroyWeaponHiglights()
    {
        destroyTiles(weaponHighlights, validTiles);
    }

    public void destroyMovementHiglights()
    {
        destroyTiles(movementHighlights, validTiles);
    }

    private void destroySkillHiglights()
    {
        destroyTiles(skillHighlight, validTiles);
        skillHighlight.Clear();
    }

    private void destroyGiveHiglights()
    {
        destroyTiles(giveHighlights, validTiles);
        giveHighlights.Clear();
    }

    private void destroyPushHiglights()
    {
        destroyTiles(pushHighlights, validTiles);
        pushHighlights.Clear();
    }

    private void destroyTiles(List<GameObject> highlights, List<Tile> tiles)
    {
        for (int i = 0; i < highlights.Count; i++)
        {
            Destroy(highlights[i]);
        }
        if (tiles != null)
        {
            tiles.Clear();
        }
    }

    public void healingItemMode(Item newCurrentItem)
    {
        if (currentItem is Weapon)
        {
            ((Weapon)currentItem).weaponModel.transform.position = getItemSlotObject(currentItem.inactiveSpot).transform.position; //TODO animations and sounds
            ((Weapon)currentItem).weaponModel.transform.rotation = getItemSlotObject(currentItem.inactiveSpot).transform.rotation;
            ((Weapon)currentItem).weaponModel.transform.SetParent(getItemSlotObject(currentItem.inactiveSpot).transform);
        }
        else if (currentItem is HealingItem)
        {
            ((HealingItem)currentItem).itemModel.transform.position = getItemSlotObject(currentItem.inactiveSpot).transform.position;
            ((HealingItem)currentItem).itemModel.transform.rotation = getItemSlotObject(currentItem.inactiveSpot).transform.rotation;
            ((HealingItem)currentItem).itemModel.transform.SetParent(getItemSlotObject(currentItem.inactiveSpot).transform);
        }
        currentItem = newCurrentItem;
        ((HealingItem)currentItem).itemModel.transform.position = getItemSlotObject(currentItem.holdingSpot).transform.position;
        ((HealingItem)currentItem).itemModel.transform.rotation = getItemSlotObject(currentItem.holdingSpot).transform.rotation;
        ((HealingItem)currentItem).itemModel.transform.SetParent(getItemSlotObject(currentItem.holdingSpot).transform);

        DestroyWeaponHiglights();
        for (int i = 0; i < weaponAreaEffectHighlights.Count; i++)
        {
            Destroy(weaponAreaEffectHighlights[i]);
        }
        setActionMode(true);

        _battleGroundController.changeBeltMode();
    }

    public void skillMode(string skillName)
    {
        if (skillHighlight.Count > 0)
        {
            destroySkillHiglights();
            setActionMode(false);
        }
        else
        {
            _battleGroundController.changeSkillMode();
            destroyMovementHiglights();
            DestroyWeaponHiglights();
            destroyPushHiglights();
            destroyGiveHiglights();

            for (int i = 0; i < skills.Count; i++)
            {
                if (skills[i].Name.Equals(skillName))
                {
                    currentSkill = skills[i];
                }
            }

            if (currentSkill == null)
            {
                return;
            }

            validTiles = TileHighlight.FindHighlight(TileMap.getTile((int)coordinates.x, (int)coordinates.z), currentSkill.Range, true, false);
            for (int i = 0; i < validTiles.Count; i++)
            {
                int x = Mathf.FloorToInt(validTiles[i].PosX / _tileMapBuilder.tileSize);
                int z = Mathf.FloorToInt(validTiles[i].PosY * (-1) / _tileMapBuilder.tileSize);  //* -1 because battleGround generates on negative z TODO
                if (validTiles[i].Equals(currentScriptedTileTarget))
                {
                    movementHighlights.Add(createPlane(x, z, new Color(1.0f, 0.0f, 0.0f, 0.3f)));
                }
                skillHighlight.Add(createPlane(x, z, new Color(0.2f, 0.2f, 0.85f, 0.4f)));
            }
        }
    }

    public void giveMode()
    {
        if (giveHighlights.Count > 0)
        {
            destroyGiveHiglights();
            setActionMode(false);
        }
        else
        {
            destroyMovementHiglights();
            DestroyWeaponHiglights();
            destroyPushHiglights();
            destroySkillHiglights();

            validTiles = TileHighlight.FindHighlight(TileMap.getTile((int)coordinates.x, (int)coordinates.z), 1, true, false);
            for (int i = 0; i < validTiles.Count; i++)
            {
                int x = Mathf.FloorToInt(validTiles[i].PosX / _tileMapBuilder.tileSize);
                int z = Mathf.FloorToInt(validTiles[i].PosY * (-1) / _tileMapBuilder.tileSize);  //* -1 because battleGround generates on negative z TODO
                giveHighlights.Add(createPlane(x, z, new Color(0.2f, 0.2f, 0.85f, 0.4f)));
            }
        }
    }

    public void pushMode()
    {
        if (pushHighlights.Count > 0)
        {
            destroyPushHiglights();
            setActionMode(false);
        }
        else
        {
            destroyMovementHiglights();
            DestroyWeaponHiglights();
            destroyGiveHiglights();
            destroySkillHiglights();

            validTiles = TileHighlight.FindHighlight(TileMap.getTile((int)coordinates.x, (int)coordinates.z), 1, true, false);
            for (int i = 0; i < validTiles.Count; i++)
            {
                int x = Mathf.FloorToInt(validTiles[i].PosX / _tileMapBuilder.tileSize);
                int z = Mathf.FloorToInt(validTiles[i].PosY * (-1) / _tileMapBuilder.tileSize);  //* -1 because battleGround generates on negative z TODO
                pushHighlights.Add(createPlane(x, z, new Color(0.85f, 0.85f, 0.5f, 0.3f)));
            }
        }
    }

    public string switchWeapon()
    {
        if (weapons.Count > 1)
        {
            for (int i = 0; i < weapons.Count; i++)
            {
                if (weapons[i] != currentItem)
                {
                    if (currentItem is Weapon)
                    {
                        ((Weapon)currentItem).weaponModel.transform.position = getItemSlotObject(currentItem.inactiveSpot).transform.position; //TODO animations and sounds
                        ((Weapon)currentItem).weaponModel.transform.rotation = getItemSlotObject(currentItem.inactiveSpot).transform.rotation;
                        ((Weapon)currentItem).weaponModel.transform.SetParent(getItemSlotObject(currentItem.inactiveSpot).transform);
                    }
                    else if (currentItem is HealingItem)
                    {
                        ((HealingItem)currentItem).itemModel.transform.position = getItemSlotObject(currentItem.inactiveSpot).transform.position;
                        ((HealingItem)currentItem).itemModel.transform.rotation = getItemSlotObject(currentItem.inactiveSpot).transform.rotation;
                        ((HealingItem)currentItem).itemModel.transform.SetParent(getItemSlotObject(currentItem.inactiveSpot).transform);
                    }
                    weapons[i].weaponModel.transform.position = getItemSlotObject(weapons[i].holdingSpot).transform.position;
                    weapons[i].weaponModel.transform.rotation = getItemSlotObject(weapons[i].holdingSpot).transform.rotation;
                    weapons[i].weaponModel.transform.SetParent(getItemSlotObject(weapons[i].holdingSpot).transform);
                    currentItem = weapons[i];

                    if (isActionMode && !isActionUsed)
                    {
                        DestroyWeaponHiglights();
                        highlightWeaponRange();
                    }
                    return currentItem.name;
                }
            }
        }
        return "Empty";
    }

    private GameObject getItemSlotObject(string slotName)
    {
        for (int i = 0; i < itemSlots.Count; i++)
        {
            if (itemSlots[i].name.Equals(slotName))
            {
                return itemSlots[i];
            }
        }
        Debug.Log("WARNING: Didn't find item slot!");
        return itemSlots[0];
    }

    public void updateHeroUnitsStats()
    {
        associatedUnit.CurrentHealth = health;
        associatedUnit.TotalExperience = totalExperience;
    }


    public void defendingPosition()
    {
        deactivatePlayerUnit();
        isSelected = true;
        movesLeft = 0;
        isActionUsed = true;
        defending = true;
    }

    public void setEnteringPosition(int x, int z)
    {
        List<Tile> path = TilePathFinder.FindPath(TileMap.getTile((int)coordinates.x, (int)coordinates.z), TileMap.getTile(x, z), true);
        for (int i = 0; i < path.Count; i++)
        {
            positionQueue.Add(new Vector3(path[i].PosX, coordinates.y, path[i].PosY));
        }
        StartCoroutine(delayedEntering());
    }

    public void setPlayerUnitActive()
    {
        setActionMode(false);
        isSelected = true;
        showMoves = true;
        if (isActionUsed)
        {
            _battleGroundController.blockUIActionsForUnit("");
        }
        else
        {
            _battleGroundController.unlockUIElements();
        }
    }

    public void setPlayerUnitActive(int x, int y)
    {
        currentScriptedTileTarget = TileMap.getTile(x, y);

        List<Tile> path = TilePathFinder.FindPath(TileMap.getTile((int)coordinates.x, (int)coordinates.z), currentScriptedTileTarget, false);
        if (path.Count > movesLeft)
        {
            movesLeft = path.Count;
        }

        setActionMode(false);
        isSelected = true;
        showMoves = true;
    }

    public void setPlayerUnitSkillActive(string skillName, int x, int y)
    {
        currentScriptedTileTarget = TileMap.getTile(x, y);
        //StartCoroutine(delayedSetSkillAction(skillName, x, y));
    }

    public void deactivatePlayerUnit()
    {
        isSelected = false;
        showMoves = false;
        setActionMode(false);
        destroyMovementHiglights();
    }

    public void resetAfterTurn()
    {
        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].CurrentCooldown > 0)
            {
                skills[i].CurrentCooldown--;
            }
        }
        if (skillChargingCounter > 0)
        {
            skillChargingCounter--;
            if (skillChargingCounter == 0)
            {
                isSkillCharged = true;
                useSkill = true;
                resetAfterTurn();
            }
        }
        else
        {
            destroyMovementHiglights();
            standardReset();
            calculateDefendBonus(false);
            isActionUsed = false;
            defending = false;
            setActionMode(false);
        }
    }

    IEnumerator delayedSetActive()
    {
        yield return new WaitForSeconds(2.0f);
        setPlayerUnitActive();
    }

    IEnumerator delayedEntering()
    {
        enteringPosition = true; //TODO remove?
        yield return new WaitForSeconds(2.0f);
    }

    IEnumerator idleAfterAttack()
    {
        yield return new WaitForSeconds(2.5f);
        anim.SetTrigger("idleTrigger");
        anim.SetFloat("idleSelect", 0.1f);
        anim.SetFloat("walkSelect", 0f);
        _battleGroundController.playerUnitActionsBlocked = false;
        _battleGroundController.setCameraMovementBlock(false);
    }

    IEnumerator delayedSetSkillAction(string skillName, int x, int y)
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 20; i++)
        {
            if (!moving)
            {
                currentScriptedTileTarget = TileMap.getTile(x, y);
                //skillMode(skillName);
                isSelected = true;
                break;
            }
            else
            {
                yield return new WaitForSeconds(0.25f);
            }
        }
    }

}
