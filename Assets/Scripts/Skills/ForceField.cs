﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : Skill
{

    UnityEngine.Object forceField;

    public ForceField() : base()
    {
        name = "Force Field";
        description = "Blocky thingy";
        levelRequired = 1;
        classRequired = "Mage";
        range = 5;
        areOfEffect = generateAreaOfEffect();
        skillType = "block";
        forceField = Resources.Load("Prefabs/ForceField");
        unitCastingStun = 1;
        cooldown = 2;
        castingType = "Charging"; //Instant
    }

    public override void useSkill(Tile tile)
    {
        currentCooldown = cooldown;
        List<Tile> affectedTiles = new List<Tile>();
        affectedTiles = TileMap.GetListOfAdjacentTiles(tile.PosX, tile.PosY);
        affectedTiles.Add(tile);

        for (int i = 0; i < affectedTiles.Count; i++)
        {
            if (!affectedTiles[i].IsUnitOnIt)
            {
                TileMap.setTileNotWalkable(affectedTiles[i].PosX, -affectedTiles[i].PosY);
                GameObject forceFieldPrefab = (GameObject)GameObject.Instantiate(forceField, new Vector3(affectedTiles[i].PosX + 0.5f, 0.5f, -affectedTiles[i].PosY + 0.5f), Quaternion.Euler(new Vector3(-90, 0, 0)));
            }
        }
    }

    private List<Vector3> generateAreaOfEffect()
    {
        List<Vector3> areOfEffect = new List<Vector3>();
        areOfEffect.Add(new Vector3(-1.0f, 0.0f, 0.0f));
        areOfEffect.Add(new Vector3(0.0f, 0.0f, 0.0f));
        areOfEffect.Add(new Vector3(1.0f, 0.0f, 0.0f));
        areOfEffect.Add(new Vector3(0.0f, 1.0f, 0.0f));
        areOfEffect.Add(new Vector3(0.0f, -1.0f, 0.0f));
        return areOfEffect;
    }


    public List<Tile> getSkillHighlights(int x, int y)
    {
        /*List<Tile> highligts = new List<Tile>();
        for (int i = 0; i < pattern.Count; i++)
        {
            Tile tile = TileMap.getTile(x + (int)pattern[i].x, y + (int)pattern[i].z);
            if (tile != null && !highligts.Contains(tile) && (tile.IsWalkable || tile.IsUnitOnIt))
            {
                highligts.Add(tile);
            }
        }*/
        return null;
    }


}
