﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MissionWorldItem : MonoBehaviour
{

    private Mission assignedMission;
    private Text missionName;


    public void setMissionWorldItem(Mission mission)
    {
        assignedMission = mission;
        missionName = GetComponentInChildren<Text>();
        missionName.text = assignedMission.getMissionName();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void performMissionLogEvent()
    {
        transform.parent.parent.parent.GetComponent<MissionsJournalController>().setMissionPreview(assignedMission);
    }
}
