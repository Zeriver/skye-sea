﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class BattleDialogController : MonoBehaviour
{

    public Canvas canvas;
    public RawImage unitPortrait;
    public Text unitName;
    public Text unitText;

    void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    void Update()
    {

    }

    public void setNewText(string text)
    {
        string[] texts = Regex.Split(text, ":");


        unitName.text = texts[0];
        unitPortrait.texture = Resources.Load("Character_Portraits/" + unitName.text + "_portrait") as Texture2D;
        this.unitText.text = texts[1];
    }


    public void changeDialogCanvasEnabled()
    {
        canvas.enabled = !canvas.enabled;
    }

}
