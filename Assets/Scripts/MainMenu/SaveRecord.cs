﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class SaveRecord : MonoBehaviour
{

    public string saveName;
    public string savePath;
    public World world;

    public SaveRecord(string saveName, World world)
    {
        this.saveName = saveName;
        this.world = world;
    }

    public string getSavePath()
    {
        return savePath;
    }

    public void setSaveRecordData(string savePath)
    {
        this.savePath = savePath;
        int lastSpaceIndex = savePath.LastIndexOf('/');
        saveName = savePath.Substring(lastSpaceIndex + 1);
        gameObject.transform.GetChild(0).GetComponent<Text>().text = saveName;
    }

    public void loadGame()
    {
        if (SceneManager.GetActiveScene().name.Equals("MainMenu"))
        {
            GameManager.instance.isGameLoaded = true;
            GameManager.instance.loadSavedInfo(savePath);
        }
        else
        {
            GameObject.Find("SaveLoadGamePanel").GetComponent<SaveLoadPanel>().setSaveSlot(this);
            if (saveName.Equals("Create New Save"))
            {
                GameObject.Find("SaveLoadGamePanel").GetComponent<SaveLoadPanel>().showCreateNewSave();
            }
        }
    }

}
