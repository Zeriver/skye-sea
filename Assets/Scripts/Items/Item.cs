﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public string name;
    public Sprite sprite;
    public GameObject objectItem;
    public string description;
    public int range;
    public bool isStackable;
    public int price;
    public string heroOwner;
    public string holdingSpot;
    public string inactiveSpot;
}
