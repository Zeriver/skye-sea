﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EquipmentController : MonoBehaviour
{

    private GameObject itemsPanel;
    private GameObject unitSilhouette;
    private Text heroName;
    private GameObject skillsPanel;

    private Unit currentUnit;
    private int unitIndex;
    private string currentViewName;

    UnityEngine.Object _item;

    void Start()
    {
        itemsPanel = transform.GetChild(0).GetChild(0).gameObject;
        unitSilhouette = transform.GetChild(0).GetChild(1).gameObject;
        heroName = transform.GetChild(0).GetChild(2).GetComponent<Text>();
        skillsPanel = transform.GetChild(0).GetChild(3).gameObject;

        unitIndex = 0;
        _item = Resources.Load("Prefabs/ItemView");
    }

    void Update()
    {

    }

    public void equipmentWindowAction()
    {
        transform.GetChild(0).gameObject.SetActive(!transform.GetChild(0).gameObject.activeSelf);

        currentUnit = GameManager.instance.getWorld().getHeroes()[unitIndex];
        if (transform.GetChild(0).gameObject.activeSelf)
        {
            resetEquipmentPanels();
            populateEquipmentPanels();
            populateHeroItems();
            heroName.text = currentUnit.getName();
        }
    }

    private void resetEquipmentPanels()
    {
        for (int i = 0; i < itemsPanel.transform.childCount; i++)
        {
            Destroy(itemsPanel.transform.GetChild(i).gameObject);
        }
    }

    private void populateEquipmentPanels()
    {
        for (int i = 0; i < GameManager.instance.getWorld().getItems().Count; i++)
        {
            if (GameManager.instance.getWorld().getItems()[i].heroOwner != null && !GameManager.instance.getWorld().getItems()[i].heroOwner.Equals(""))
            {
                continue;
            }
            else
            {
                GameObject itemPrefab = (GameObject)GameObject.Instantiate(_item);
                itemPrefab.transform.SetParent(itemsPanel.transform);
                itemPrefab.GetComponent<ItemWorld>().setAllInfo(GameManager.instance.getWorld().getItems()[i]);
            }
        }
    }

    private void resetHeroEquipment()
    {
        for (int i = 0; i < unitSilhouette.transform.childCount; i++)
        {
            unitSilhouette.transform.GetChild(i).GetComponent<EquippedItem>().resetEquipment();
        }
    }

    private void populateHeroItems()
    {
        for (int i = 0; i < GameManager.instance.getWorld().getItems().Count; i++)
        {
            if (GameManager.instance.getWorld().getItems()[i].heroOwner.Equals(currentUnit.getName()))
            {
                equipItem(GameManager.instance.getWorld().getItems()[i], GameManager.instance.getWorld().getItems()[i].holdingSpot, false);
            }
        }
    }

    public void equipItem(Item item, string holdingSpot, bool shouldUnequip)
    {
        for (int i = 0; i < unitSilhouette.transform.childCount; i++)
        {
            if (unitSilhouette.transform.GetChild(i).name.Equals(holdingSpot))
            {
                if (shouldUnequip && unitSilhouette.transform.GetChild(i).GetComponent<EquippedItem>().getAssignedItem() != null)
                {
                    unitSilhouette.transform.GetChild(i).GetComponent<EquippedItem>().unequipItem();
                }
                unitSilhouette.transform.GetChild(i).GetComponent<EquippedItem>().setEquippedItem(item);
            }
        }
        item.heroOwner = currentUnit.getName();
    }

    public void unequipItem(Item item)
    {
        GameObject itemPrefab = (GameObject)GameObject.Instantiate(_item);
        itemPrefab.transform.SetParent(itemsPanel.transform);
        itemPrefab.GetComponent<ItemWorld>().setAllInfo(item);
        item.heroOwner = "";
    }

    public void changeHero(int number)
    {
        if ((number < 0 && unitIndex > 0) || (number > 0 && unitIndex + 1 < GameManager.instance.getWorld().getHeroes().Count))
        {
            unitIndex += number;
            currentUnit = GameManager.instance.getWorld().getHeroes()[unitIndex];
            heroName.text = currentUnit.getName();
            resetHeroEquipment();
            populateHeroItems();
            skillsPanel.GetComponent<SkillsPanel>().populateUnitSkills(currentUnit);
        }
    }

    public Unit getCurrentUnit()
    {
        return currentUnit;
    }

    public void changeView(string viewName)
    {
        currentViewName = viewName;
        if (viewName.Equals("Equipment"))
        {
            itemsPanel.SetActive(true);
            skillsPanel.SetActive(false);
        }
        else if (viewName.Equals("Skills"))
        {
            itemsPanel.SetActive(false);
            skillsPanel.SetActive(true);
            skillsPanel.GetComponent<SkillsPanel>().populateUnitSkills(currentUnit);
        }
    }
}
