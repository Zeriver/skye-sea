﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneHandedAxe : Weapon
{

    public OneHandedAxe() : base()
    {
        name = "OneHandedAxe";
        description = "Chop chop!";
        damageType = "cut";
        damage = 10;
        cooldown = 0;
        range = 1;
        isDiagonal = false;
        isFlankingBonus = true;
        isMelee = true;
        heroOwner = "";
        holdingSpot = "RightHandSlot";
        inactiveSpot = "BackSlot";
        price = 250;
    }

    public OneHandedAxe(string owner) : base()
    {
        name = "OneHandedAxe";
        description = "Chop chop!";
        damageType = "cut";
        damage = 10;
        cooldown = 0;
        range = 1;
        isDiagonal = false;
        isFlankingBonus = true;
        isMelee = true;
        heroOwner = owner;
        holdingSpot = "RightHandSlot";
        inactiveSpot = "BackSlot";
        price = 250;
    }
}
