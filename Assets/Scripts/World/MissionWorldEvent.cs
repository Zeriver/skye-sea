﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionWorldEvent : MonoBehaviour
{

    private string dialogID;
    private Mission assignedMission;

    private bool isShowAnimation;

    public void setMissionWorldEvent(string dialogID, Mission assignedMission)
    {
        this.dialogID = dialogID;
        this.assignedMission = assignedMission;
    }

    void Start()
    {
        isShowAnimation = true;
    }

    void Update()
    {
        if (isShowAnimation)
        {
            transform.localScale += new Vector3(0.01f, 0.01f, 0.0f);
            if (transform.localScale.x >= 1)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
                isShowAnimation = false;
            }
        }
    }

    public void performMissionEvent()
    {
        assignedMission.incrementMissionState(assignedMission); //TODO temporary
        GameObject.Find("DialogCanvas").transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("DialogCanvas").GetComponent<DialogController>().setDialogs(dialogID);
        transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
    }
}
