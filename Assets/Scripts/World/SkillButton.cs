﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillButton : MonoBehaviour, IPointerEnterHandler
{

    private Button button;
    private SkillsPanel skillsPanel;
    private string skillName;
    private Unit currentUnit;

    void Start()
    {

    }

    void Update()
    {

    }

    public void setButtonSkill(SkillsPanel skillsPanel, Unit currentUnit)
    {
        this.skillsPanel = skillsPanel;
        this.currentUnit = currentUnit;
        skillName = gameObject.name;
        button = GetComponent<Button>();

        if (skillsPanel.createSkillForUnit(skillName) != null &&
            skillsPanel.createSkillForUnit(skillName).LevelRequired > currentUnit.Level)
        {
            button.interactable = false;
        }
    }

    public void setButtonSkillActive()
    {
        button = GetComponent<Button>();
        button.interactable = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        skillsPanel.showDescription(skillName);
    }

    public void activateSkill()
    {
        if (currentUnit.SkillPoints > 0)
        {
            currentUnit.SkillPoints--;
            button.interactable = false;
            skillsPanel.addSkill(skillName);
        }
    }

}
