﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    private bool isFlyingIn;
    private bool isInPosition;
    private bool isGameLoaded;
    private Vector3 velocity = Vector3.zero;

    private GameObject shipStartingPosition;
    private GameObject portingPosition;
    private GameObject currentTarget;

    public float maxVel = 15;
    public float startVel = 9;
    public float stopVel = 0.3f;
    public float accDistance = 20;
    public float factor = 0.25f; // max inclination
    public float turnSpeed = 0.8f;
    Vector3 lastPos;
    bool flying = false;
    float positionAdjuster;

    private List<SailsController> sails;

    void Start()
    {
        shipStartingPosition = GameObject.Find("ShipStartingPosition");
        portingPosition = GameObject.Find("ShipPortingPosition");
        sails = new List<SailsController>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<SailsController>() != null)
            {
                sails.Add(transform.GetChild(i).GetComponent<SailsController>());
            }
        }
        if (!isGameLoaded)
        {
            isFlyingIn = true;
            transform.position = shipStartingPosition.transform.position;
            transform.rotation = shipStartingPosition.transform.rotation;
            Camera.main.GetComponent<InWorldCameraController>().startPortingMove();
            StartCoroutine(FlyTo(portingPosition.transform.position));
            currentTarget = portingPosition;
        }
    }

    void Update()
    {
        Vector3 dir = transform.position - lastPos;
        lastPos = transform.position;
        float dist = dir.magnitude;
        if (dist > 0.001f && !isInPosition)
        {
            dir /= dist;
            float vel = dist / Time.deltaTime;
            Quaternion bankRot = Quaternion.LookRotation((dir * 2f) + factor * Vector3.down * vel / maxVel);
            transform.rotation = Quaternion.Slerp(transform.rotation, bankRot, turnSpeed * Time.deltaTime);

            //transform.rotation = Quaternion.RotateTowards(transform.rotation, currentTarget.transform.rotation, 20 * Time.deltaTime);
        }

        if (Vector3.Distance(transform.position, portingPosition.transform.position) < 4.5f && isFlyingIn)
        {
            isFlyingIn = false;
            GameObject.Find("WorldController").GetComponent<WorldController>().onShipDockedEvents();
            for (int i = 0; i < sails.Count; i++)
            {
                sails[i].stopSails();
            }
        }

        if (Vector3.Distance(transform.position, portingPosition.transform.position) == 0.0f || isInPosition)
        {
            if (!isInPosition)
            {
                positionAdjuster = Mathf.PingPong(Time.time, 5);
                Camera.main.GetComponent<InWorldCameraController>().setIslandView();
            }
            isInPosition = true;
            Vector3 nextStepPosition = new Vector3(transform.position.x, (portingPosition.transform.position.y - positionAdjuster) + (Mathf.PingPong(Time.time, 5)), transform.position.z);
            transform.position = Vector3.Slerp(transform.position, nextStepPosition, 2 * Time.deltaTime);
        }
    }

    IEnumerator FlyTo(Vector3 targetPos)
    {
        flying = true;
        Vector3 startPos = transform.position;
        Vector3 dir = targetPos - startPos;
        float distTotal = dir.magnitude;
        dir /= distTotal;
        float accDist = Mathf.Min(accDistance, distTotal / 2);
        do
        {
            float dist1 = Vector3.Distance(transform.position, startPos);
            float dist2 = distTotal - dist1;
            float speed = maxVel;
            if (dist1 < accDist)
            {
                speed = Mathf.Lerp(startVel, maxVel, dist1 / accDist);
            }
            else
            if (dist2 < accDist)
            {
                speed = Mathf.Lerp(stopVel, maxVel, dist2 / accDist);
            }
            if (!isInPosition)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
            }
            yield return 0;
        } while (transform.position != targetPos);
        flying = false;
    }


    public void setValuesOnLoadGame()
    {
        isGameLoaded = true;
        shipStartingPosition = GameObject.Find("ShipStartingPosition");
        portingPosition = GameObject.Find("ShipPortingPosition");
        isFlyingIn = false;
        isInPosition = true;
        transform.position = portingPosition.transform.position;
        transform.rotation = portingPosition.transform.rotation;
        Camera.main.GetComponent<InWorldCameraController>().setValuesOnLoadGame();
    }

    public void setFlyAwayValues()
    {
        isInPosition = false;
        StartCoroutine(FlyTo(shipStartingPosition.transform.position));
        currentTarget = shipStartingPosition;
        Camera.main.GetComponent<InWorldCameraController>().setFlyLeaveValues();
        for (int i = 0; i < sails.Count; i++)
        {
            sails[i].startSails();
        }
    }

}
