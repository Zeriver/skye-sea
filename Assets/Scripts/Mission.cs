﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Mission : IXmlSerializable
{
    private string missionID;
    private string missionName;
    private string missionDescription;

    private string currentState;
    private string nextEventLocation;
    private string[] missionScenes;
    private string[] dialogIds;
    private int scenesIndex;

    //TODO Rewards, requirments etc

    public Mission()
    {

    }

    public Mission(string missionID, string missionName, string missionDescription, string nextEventLocation, string[] missionScenes, string[] dialogIds)
    {
        this.missionID = missionID;
        this.missionName = missionName;
        this.missionDescription = missionDescription;
        this.nextEventLocation = nextEventLocation;
        this.missionScenes = missionScenes;
        this.dialogIds = dialogIds;

        currentState = "STARTED";
        scenesIndex = 0;
    }

    public Mission(string missionID, string missionName, string missionDescription, string nextEventLocation, string[] missionScenes, string[] dialogIds, string currentState, int scenesIndex)
    {
        this.missionID = missionID;
        this.missionName = missionName;
        this.missionDescription = missionDescription;
        this.nextEventLocation = nextEventLocation;
        this.missionScenes = missionScenes;
        this.dialogIds = dialogIds;
        this.currentState = currentState;
        this.scenesIndex = scenesIndex;
    }

    public Mission(string missionID) // For Single Dialog Events Only
    {
        this.missionID = missionID;
    }

    public string getMissionID()
    {
        return missionID;
    }

    public string getMissionName()
    {
        return missionName;
    }

    public string getMissionDescription()
    {
        return missionDescription;
    }

    public string getNextEventLocation()
    {
        return nextEventLocation;
    }

    public string[] getMissionScenes()
    {
        return missionScenes;
    }

    public string getMissionId()
    {
        return dialogIds[scenesIndex];
    }

    public void incrementMissionState(Mission assignedMission)
    {
        scenesIndex++;
        if (scenesIndex < missionScenes.Length)
        {
            nextEventLocation = missionScenes[scenesIndex];
        }
        else
        {
            nextEventLocation = "NONE";
            currentState = "COMPLETED";
            GameManager.instance.getWorld().completeMission(assignedMission);
        }
    }

    public XmlSchema GetSchema()
    {
        return null;
    }

    public void WriteXml(XmlWriter writer)
    {
        Debug.Log("Writing UNIT XML");

        writer.WriteAttributeString("missionID", missionID);
        writer.WriteAttributeString("missionName", missionName);
        writer.WriteAttributeString("missionDescription", missionDescription);
        writer.WriteAttributeString("currentState", currentState);
        writer.WriteAttributeString("nextEventLocation", nextEventLocation);
        writer.WriteAttributeString("scenesIndex", scenesIndex.ToString());

        if (missionScenes != null)
        {
            for (int i = 0; i < missionScenes.Length; i++)
            {
                writer.WriteAttributeString("MissionScene" + i, missionScenes[i]);
            }
        }
        if (dialogIds != null)
        {
            for (int i = 0; i < dialogIds.Length; i++)
            {
                writer.WriteAttributeString("dialogId" + i, dialogIds[i]);
            }
        }
    }

    public void ReadXml(XmlReader reader)
    {

    }
}
