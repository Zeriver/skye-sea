﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TransactionController : MonoBehaviour
{
    private ItemWorld currentlySelectedItem;
    private bool isPlayerItem;

    private GameObject playerEquipmentPanel;
    private GameObject merchantInventoryPanel;
    private GameObject transactionButton;
    private Text playerMoney;

    UnityEngine.Object _item;

    void Start()
    {
        playerEquipmentPanel = transform.GetChild(0).GetChild(0).gameObject;
        merchantInventoryPanel = transform.GetChild(0).GetChild(1).gameObject;
        transactionButton = transform.GetChild(0).GetChild(2).gameObject;
        playerMoney = transform.GetChild(0).GetChild(3).GetComponent<Text>();


        _item = Resources.Load("Prefabs/ItemView");
    }

    void Update()
    {

    }

    public void setCurrentlySelectedItem(ItemWorld item, bool isPlayerItem)
    {
        currentlySelectedItem = item;
        this.isPlayerItem = isPlayerItem;
        if (isPlayerItem)
        {
            transactionButton.GetComponentInChildren<Text>().text = "SELL";
        }
        else
        {
            transactionButton.GetComponentInChildren<Text>().text = "BUY";
        }
    }

    public void transactionWindowAction()
    {
        transform.GetChild(0).gameObject.SetActive(!transform.GetChild(0).gameObject.activeSelf);
        if (transform.GetChild(0).gameObject.activeSelf)
        {
            playerMoney.text = GameManager.instance.getWorld().getMoney().ToString();
            resetPanels();
            populateTransactionPanels();
        }
    }

    private void resetPanels()
    {
        for (int i = 0; i < playerEquipmentPanel.transform.childCount; i++)
        {
            Destroy(playerEquipmentPanel.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < merchantInventoryPanel.transform.childCount; i++)
        {
            Destroy(merchantInventoryPanel.transform.GetChild(i).gameObject);
        }
        transactionButton.GetComponentInChildren<Text>().text = "";
        currentlySelectedItem = null;
    }

    private void populateTransactionPanels()
    {
        for (int i = 0; i < GameManager.instance.getWorld().getItems().Count; i++)
        {
            if (GameManager.instance.getWorld().getItems()[i].heroOwner != null && !GameManager.instance.getWorld().getItems()[i].heroOwner.Equals(""))
            {
                continue;
            }
            GameObject itemPrefab = (GameObject)GameObject.Instantiate(_item);
            itemPrefab.transform.SetParent(playerEquipmentPanel.transform);
            itemPrefab.GetComponent<ItemWorld>().setAllInfo(GameManager.instance.getWorld().getItems()[i]);
        }

        for (int i = 0; i < GameManager.instance.getWorld().getWorldMerchantItems().Count; i++)
        {
            GameObject itemPrefab = (GameObject)GameObject.Instantiate(_item);
            itemPrefab.transform.SetParent(merchantInventoryPanel.transform);
            itemPrefab.GetComponent<ItemWorld>().setAllInfo(GameManager.instance.getWorld().getWorldMerchantItems()[i]);
        }
    }

    public void performTransaction()
    {
        if (currentlySelectedItem != null)
        {
            transactionButton.GetComponentInChildren<Text>().text = "";
            if (isPlayerItem)
            {
                GameManager.instance.getWorld().setMoney(GameManager.instance.getWorld().getMoney() + currentlySelectedItem.getRealPrice());
                GameManager.instance.getWorld().getItems().Remove(currentlySelectedItem.getAssignedItem());
                GameManager.instance.getWorld().getWorldMerchantItems().Add(currentlySelectedItem.getAssignedItem());
                currentlySelectedItem.transform.SetParent(merchantInventoryPanel.transform);
            }
            else
            {
                if (GameManager.instance.getWorld().getMoney() >= currentlySelectedItem.getRealPrice())
                {
                    GameManager.instance.getWorld().setMoney(GameManager.instance.getWorld().getMoney() - currentlySelectedItem.getRealPrice());
                    GameManager.instance.getWorld().getWorldMerchantItems().Remove(currentlySelectedItem.getAssignedItem());
                    GameManager.instance.getWorld().getItems().Add(currentlySelectedItem.getAssignedItem());
                    currentlySelectedItem.transform.SetParent(playerEquipmentPanel.transform);
                }
                else
                {
                    transactionButton.GetComponentInChildren<Text>().text = "Not enough money!";
                }
            }
            
            currentlySelectedItem.updatePrice();
            playerMoney.text = GameManager.instance.getWorld().getMoney().ToString();

            currentlySelectedItem = null;
        }
    }
}
