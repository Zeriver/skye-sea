﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MissionsJournalController : MonoBehaviour
{

    private GameObject missionDescriptionPanel;
    private GameObject missionListPanel;
    private Button currentMissionsButton;
    private Button completedMissionsButton;
    private bool isCurrentMissionList;
    
    UnityEngine.Object _missionItem;

    void Start()
    {
        missionDescriptionPanel = transform.GetChild(0).GetChild(0).gameObject;
        missionListPanel = transform.GetChild(0).GetChild(1).gameObject;
        currentMissionsButton = transform.GetChild(0).GetChild(2).GetComponent<Button>();
        completedMissionsButton = transform.GetChild(0).GetChild(3).GetComponent<Button>();
        currentMissionsButton.interactable = false;
        isCurrentMissionList = true;

        _missionItem = Resources.Load("Prefabs/MissionLogItem");
    }

    void Update()
    {

    }

    public void missionJournalWindowAction()
    {
        transform.GetChild(0).gameObject.SetActive(!transform.GetChild(0).gameObject.activeSelf);
        if (transform.GetChild(0).gameObject.activeSelf)
        {
            resetMissionList();
            populateMissionList();
        }
    }

    private void resetMissionList()
    {
        for (int i = 0; i < missionListPanel.transform.childCount; i++)
        {
            Destroy(missionListPanel.transform.GetChild(i).gameObject);
        }
    }

    private void populateMissionList()
    {
        List<Mission> currentMissionsList = new List<Mission>();
        if (isCurrentMissionList)
        {
            currentMissionsList = GameManager.instance.getWorld().getCurrentMissions();
        }
        else
        {
            currentMissionsList = GameManager.instance.getWorld().getCompletedMissions();
        }

        for (int i = 0; i < currentMissionsList.Count; i++)
        {
            if (currentMissionsList[i].getMissionName() == null)
            {
                continue;
            }
            GameObject missionLogPrefab = (GameObject)GameObject.Instantiate(_missionItem);
            missionLogPrefab.transform.SetParent(missionListPanel.transform);
            missionLogPrefab.GetComponent<MissionWorldItem>().setMissionWorldItem(currentMissionsList[i]);
        }
    }

    public void setMissionPreview(Mission mission)
    {
        missionDescriptionPanel.GetComponentInChildren<Text>().text = mission.getMissionDescription();
    }

    public void changeMissionList()
    {
        isCurrentMissionList = !isCurrentMissionList;
        if (isCurrentMissionList)
        {
            currentMissionsButton.interactable = false;
            completedMissionsButton.interactable = true;
        }
        else
        {
            currentMissionsButton.interactable = true;
            completedMissionsButton.interactable = false;
        }
        resetMissionList();
        populateMissionList();
        missionDescriptionPanel.GetComponentInChildren<Text>().text = "";
    }
}
