﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : Weapon
{
    public Bow() : base()
    {
        name = "Bow";
        description = "Regural stabbing... but at range!";
        damageType = "thrust";
        damage = 10;
        cooldown = 0;
        range = 5;
        isDiagonal = false;
        isFlankingBonus = true;
        isMelee = false;
        heroOwner = "";
        holdingSpot = "RightHandSlot";
        inactiveSpot = "BackSlot";
        price = 200;
        additionalItem = "Quiver";
        additionalItemSpot = "BackSlot";
    }

    public Bow(string owner, int ammunition) : base()
    {
        name = "Bow";
        description = "Regural stabbing... but at range!";
        this.ammunition = ammunition;
        damageType = "thrust";
        damage = 10;
        cooldown = 0;
        range = 5;
        isDiagonal = false;
        isFlankingBonus = true;
        isMelee = false;
        heroOwner = owner;
        holdingSpot = "RightHandSlot";
        inactiveSpot = "BackSlot";
        price = 200;
        additionalItem = "Quiver";
        additionalItemSpot = "BackSlot";
    }
}
