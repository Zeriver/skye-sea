﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

public class Unit : IXmlSerializable
{
    protected string name;
    protected string className;
    protected int maxHealth;
    protected int currentHealth;
    protected int maxMovement;
    protected int totalExperience;
    protected int level;
    protected int skillPoints;
    protected List<Skill> skills;

    protected int fireResistance;
    protected int freezeResistance;
    protected int poisonResistance;
    protected int defendBonus;


    public Unit()
    {

    }

    public Unit(string name, string className)
    {
        this.name = name;
        this.className = className;

        maxHealth = 100;
        currentHealth = 100;
        maxMovement = 5;
        totalExperience = 0;


        fireResistance = 0;
        freezeResistance = 0;
        poisonResistance = 0;
        defendBonus = 0;
    }

    public Unit(string name, string className, int maxHealth, int currentHealth, int maxMovement, int totalExperience, int level, int skillPoints, List<Skill> skills)
    {
        this.name = name;
        this.className = className;

        this.maxHealth = maxHealth;
        this.currentHealth = currentHealth;
        this.maxMovement = maxMovement;
        this.totalExperience = totalExperience;
        this.level = level;
        this.skillPoints = skillPoints;
        this.skills = skills;

        fireResistance = 0;
        freezeResistance = 0;
        poisonResistance = 0;
        defendBonus = 0;
    }


    public string getName()
    {
        return name;
    }

    public string getClassName()
    {
        return className;
    }

    public List<Skill> getSkills()
    {
        return skills;
    }

    public void addNewSkill(Skill skill)
    {
        bool isNewSkill = true;
        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].Name.Equals(skill.Name))
            {
                isNewSkill = false;
                break;
            }
        }
        if (isNewSkill)
        {
            skills.Add(skill);
        }
    }

    public int MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    public int CurrentHealth
    {
        get { return currentHealth; }
        set { currentHealth = value; }
    }

    public int MaxMovement
    {
        get { return maxMovement; }
        set { maxMovement = value; }
    }

    public int TotalExperience
    {
        get { return totalExperience; }
        set { totalExperience = value; }
    }

    public int Level
    {
        get { return level; }
        set { level = value; }
    }

    public int SkillPoints
    {
        get { return skillPoints; }
        set { skillPoints = value; }
    }

    public int FireResistance
    {
        get { return fireResistance; }
        set { fireResistance = value; }
    }

    public int FreezeResistance
    {
        get { return freezeResistance; }
        set { freezeResistance = value; }
    }

    public int PoisonResistance
    {
        get { return poisonResistance; }
        set { poisonResistance = value; }
    }

    public int DefendBonus
    {
        get { return defendBonus; }
        set { defendBonus = value; }
    }


    public XmlSchema GetSchema()
    {
        return null;
    }

    public void WriteXml(XmlWriter writer)
    {
        Debug.Log("Writing UNIT XML");

        writer.WriteAttributeString("name", name);
        writer.WriteAttributeString("className", className);
        writer.WriteAttributeString("maxHealth", maxHealth.ToString());
        writer.WriteAttributeString("currentHealth", currentHealth.ToString());
        writer.WriteAttributeString("maxMovement", maxMovement.ToString());
        writer.WriteAttributeString("totalExperience", totalExperience.ToString());
        writer.WriteAttributeString("level", level.ToString());
        writer.WriteAttributeString("skillPoints", skillPoints.ToString());

        if (skills != null)
        {
            for (int i = 0; i < skills.Count; i++)
            {
                writer.WriteAttributeString("skill" + i, skills[i].Name);
            }
        }
    }

    public void ReadXml(XmlReader reader)
    {

    }

}
