﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class WorldController : MonoBehaviour
{

    public GameObject pausePanel;

    private string nextTravelLocation;
    private GameObject shipObject;

    private GameObject equipmentCanvas;
    private GameObject missionJournalCanvas;
    private GameObject mapPanel;
    private GameObject UICanvas;
    private Canvas fadingCanvas;
    private Image fadeImage;
    private Image topBar;
    private Image bottomBar;
    private bool isFadingOut;
    private bool isFadingIn;
    private bool isBarFadingOut;
    private bool isBarFadingIn;
    private float fadeInSpeed;

    private bool allowAction;

    UnityEngine.Object _missionEvent;

    void OnEnable()
    {
        _missionEvent = Resources.Load("Prefabs/MissionEvent");
        shipObject = GameObject.Find("Ship");
        equipmentCanvas = GameObject.Find("UnityEquipmentCanvas");
        missionJournalCanvas = GameObject.Find("MissionsJournalCanvas");
        mapPanel = GameObject.Find("MapCanvas");
        UICanvas = GameObject.Find("UICanvas");
        fadingCanvas = GameObject.Find("FadeCanvas").GetComponent<Canvas>();
        fadeImage = fadingCanvas.GetComponentInChildren<Image>();
        topBar = fadingCanvas.transform.GetChild(1).GetComponent<Image>();
        bottomBar = fadingCanvas.transform.GetChild(2).GetComponent<Image>();

        nextTravelLocation = "";

        fadeInSpeed = 0.2f;
        if (GameManager.instance.isGameLoaded)
        {
            shipObject.GetComponent<ShipController>().setValuesOnLoadGame();
            fadeInSpeed *= 3;
            onShipDockedEvents();
        }
        startFadeInTransition();

        for (int i = 0; i < mapPanel.transform.GetChild(0).childCount; i++)
        {
            if (mapPanel.transform.GetChild(0).GetChild(i).name.Equals(SceneManager.GetActiveScene().name))
            {
                mapPanel.transform.GetChild(0).GetChild(i).GetComponent<Button>().interactable = false;
            }
        }


        // Merchant Equipment TEMPORARY
        GameManager.instance.getWorld().getWorldMerchantItems().Add(new OneHandedAxe());
        GameManager.instance.getWorld().getWorldMerchantItems().Add(new OneHandedAxe());
        GameManager.instance.getWorld().getWorldMerchantItems().Add(new OneHandedAxe());

        GameManager.instance.isGameLoaded = false;
    }


    void Start()
    {

    }

    void Update()
    {
        if (isFadingOut)
        {
            if (fadeStep(fadeImage, null, fadeInSpeed) >= 0.99f)
            {
                isFadingOut = false;
                SceneManager.LoadScene(nextTravelLocation);
            }
        }
        if (isFadingIn)
        {
            if (fadeStep(fadeImage, null, -fadeInSpeed) <= 0.01f)
            {
                isFadingIn = false;
            }
        }

        if (isBarFadingOut)
        {
            if (fadeStep(bottomBar, topBar, 0.5f) >= 0.99f)
            {
                isBarFadingOut = false;
                isFadingOut = true;
            }
        }
        if (isBarFadingIn)
        {
            if (fadeStep(bottomBar, topBar, -0.5f) <= 0.01f)
            {
                isBarFadingIn = false;
                fadingCanvas.enabled = false;
            }
        }

        if (allowAction)
        {
            if (Input.GetMouseButtonDown(1))     /// RIGHT CLICK
            {
                GameManager.instance.loadBattleGrounds("SampleScene", GameManager.instance.getWorld().getHeroes());
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                mainMenuPanel();
            }
        }

    }

    private float fadeStep(Image image, Image image2, float step)
    {
        Color c = image.color;
        c.a += step * Time.deltaTime;
        image.color = c;
        if (image2)
            image2.color = c;
        return c.a;
    }

    public void travelToLocation(string sceneName)
    {
        nextTravelLocation = sceneName;
        startFadeOutTransition();
    }

    public void onShipDockedEvents()
    {
        GameObject[] worldEvents = GameObject.FindGameObjectsWithTag("WorldEvent");
        for (int i = 0; i < worldEvents.Length; i++)
        {
            worldEvents[i].GetComponent<WorldEvent>().showEvent();
        }
        showMissionEvents();
        isBarFadingIn = true;
        allowAction = true;
        StartCoroutine(delayedUIShow());
    }

    public void startFadeOutTransition()
    {
        mapPanel.transform.GetChild(0).gameObject.SetActive(false);
        fadingCanvas.enabled = true;
        isBarFadingOut = true;
        allowAction = false;
        shipObject.GetComponent<ShipController>().setFlyAwayValues();
    }

    private void startFadeInTransition()
    {
        fadingCanvas.enabled = true;
        isFadingIn = true;
        Color c = fadeImage.color;
        c.a = 1.0f;
        fadeImage.color = c;
    }

    private void showMissionEvents()
    {
        List<Mission> allMissions = new List<Mission>(GameManager.instance.getWorld().getCurrentMissions().Count + GameManager.instance.getWorld().getCompletedMissions().Count);
        allMissions.AddRange(GameManager.instance.getWorld().getCurrentMissions());
        allMissions.AddRange(GameManager.instance.getWorld().getCompletedMissions());

        for (int i = 0; i < allMissions.Count; i++)
        {
            if (allMissions[i].getNextEventLocation() == null || allMissions[i].getNextEventLocation().Equals(""))
            {
                continue;
            }
            if (allMissions[i].getNextEventLocation().Equals(SceneManager.GetActiveScene().name))
            {
                for (int j = 0; j < GameObject.FindGameObjectsWithTag("WorldEventSlot").Length; j++)
                {
                    if (GameObject.FindGameObjectsWithTag("WorldEventSlot")[j].GetComponent<Collider>().enabled)
                    {
                        GameObject missionEvent = (GameObject)GameObject.Instantiate(_missionEvent);
                        missionEvent.GetComponent<MissionWorldEvent>().setMissionWorldEvent(allMissions[i].getMissionId(), allMissions[i]);
                        missionEvent.transform.SetParent(GameObject.FindGameObjectsWithTag("WorldEventSlot")[j].transform);
                        missionEvent.transform.position = GameObject.FindGameObjectsWithTag("WorldEventSlot")[j].transform.position;

                        GameObject.FindGameObjectsWithTag("WorldEventSlot")[j].GetComponent<Collider>().enabled = false;
                        break;
                    }
                }
            }
        }
    }

    public void createNewMission(Mission mission)
    {
        GameManager.instance.getWorld().addNewMission(mission);
    }

    public void mapPanelAction()
    {
        mapPanel.transform.GetChild(0).gameObject.SetActive(!mapPanel.transform.GetChild(0).gameObject.activeSelf);
    }

    public void missionsPanelAction()
    {
        missionJournalCanvas.GetComponent<MissionsJournalController>().missionJournalWindowAction();
    }

    public void equipmentSkillPanelAction()
    {
        equipmentCanvas.GetComponent<EquipmentController>().equipmentWindowAction();
    }

    public void mainMenuPanel()
    {
        pausePanel.SetActive(!pausePanel.activeSelf);
    }

    IEnumerator delayedUIShow()
    {
        yield return new WaitForSeconds(2);
        UICanvas.transform.GetChild(0).gameObject.SetActive(true);
    }
}
