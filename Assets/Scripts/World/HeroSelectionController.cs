﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HeroSelectionController : MonoBehaviour
{

    private string battlegroundName;
    private int heroSlots;

    private List<Unit> selectedUnits;
    private List<GameObject> selectedPortraits;
    private List<GameObject> roosterPortraits;
    private int selectedIndex;

    void Start()
    {
        selectedUnits = new List<Unit>();
        selectedPortraits = new List<GameObject>();
        for (int i = 0; i < transform.GetChild(0).GetChild(0).childCount; i++)
        {
            selectedPortraits.Add(transform.GetChild(0).GetChild(0).GetChild(i).gameObject);
        }
        roosterPortraits = new List<GameObject>();
        for (int i = 0; i < transform.GetChild(0).GetChild(1).childCount; i++)
        {
            roosterPortraits.Add(transform.GetChild(0).GetChild(1).GetChild(i).gameObject);
        }
        selectedIndex = 0;
    }

    public void setHeroSelectionController(string battlegroundName, int heroSlots)
    {
        transform.GetChild(0).gameObject.SetActive(true);
        this.battlegroundName = battlegroundName;
        this.heroSlots = heroSlots;
        for (int i = 0; i < selectedPortraits.Count; i++)
        {
            if (i >= heroSlots)
            {
                selectedPortraits[i].SetActive(false);
            }
        }
    }

    void Update()
    {

    }

    public void enterBattleground()
    {
        if (selectedUnits.Count == heroSlots)
        {
            GameManager.instance.loadBattleGrounds(battlegroundName, selectedUnits);
        }
    }

    public void setSelectedHero(Button pressedButton)
    {
        if (selectedIndex < heroSlots)
        {
            string name = pressedButton.GetComponentInChildren<Text>().text;
            pressedButton.interactable = false;
            selectedUnits.Add(findHeroUnit(name));
            selectedPortraits[selectedIndex].GetComponentInChildren<Text>().text = name;
            selectedPortraits[selectedIndex].GetComponentInChildren<Button>().interactable = true;
            selectedPortraits[selectedIndex].GetComponentInChildren<RawImage>().texture = Resources.Load("Character_Portraits/" + name + "_portrait") as Texture2D;
            selectedIndex++;
        }
    }

    public void deselectHero(Button pressedButton)
    {
        string name = pressedButton.GetComponentInChildren<Text>().text;
        for (int i = 0; i < roosterPortraits.Count; i++)
        {
            if (roosterPortraits[i].GetComponentInChildren<Text>().text.Equals(name))
            {
                roosterPortraits[i].GetComponentInChildren<Button>().interactable = true;
            }
        }
        selectedUnits.Remove(findHeroUnit(name));
        selectedIndex--;
        slideSelectedAfterRemoval();
    }

    private void slideSelectedAfterRemoval()
    {
        for (int i = 0; i < selectedUnits.Count; i++)
        {
            selectedPortraits[i].GetComponentInChildren<Text>().text = selectedUnits[i].getName();
        }
        selectedPortraits[selectedIndex].GetComponentInChildren<Text>().text = "";
        selectedPortraits[selectedIndex].GetComponentInChildren<Button>().interactable = false;
        selectedPortraits[selectedIndex].GetComponentInChildren<RawImage>().texture = null;

    }

    private Unit findHeroUnit(string name)
    {
        for (int i = 0; i < GameManager.instance.getWorld().getHeroes().Count; i++)
        {
            if (GameManager.instance.getWorld().getHeroes()[i].getName().Equals(name))
            {
                return GameManager.instance.getWorld().getHeroes()[i];
            }
        }
        Debug.Log("WARNING: did not find the hero unit");
        return null; //SHOULD NEVER REACH
    }

}
