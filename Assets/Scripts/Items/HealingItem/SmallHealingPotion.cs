﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallHealingPotion : HealingItem
{
    public SmallHealingPotion(int amount) : base(amount)
    {
        name = "Small Healing Potion";
        description = "No nie 4 HP";
        this.amount = amount;
        healingPoints = 25;
        isStackable = true;
        holdingSpot = "RightHandSlot";
        inactiveSpot = "BeltRightSlot";
    }
}
