﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ItemWorld : MonoBehaviour
{
    private Item assignedItem;
    private int realPrice;

    private RawImage itemImage;
    private Text itemName;
    private Text itemDescription;

    void Start()
    {

    }

    void Update()
    {

    }

    public void setAllInfo(Item item)
    {
        itemImage = GetComponentInChildren<RawImage>();
        itemName = transform.GetChild(1).GetComponent<Text>();
        itemDescription = transform.GetChild(2).GetComponent<Text>();
        assignedItem = item;
        itemImage.texture = Resources.Load<Texture2D>("Prefabs/Textures/" + item.name + "_item");
        itemDescription.text = item.description;
        updatePrice();
    }

    public void updatePrice()
    {
        if (transform.parent.name.Contains("Player"))
        {
            realPrice = assignedItem.price / 2;
        }
        else
        {
            realPrice = assignedItem.price;
        }
        itemName.text = assignedItem.name + "     " + realPrice;
    }

    public int getRealPrice()
    {
        return realPrice;
    }

    public void performItemAction()
    {
        if (transform.parent.name.Equals("AvailableEquipment"))
        {
            transform.parent.parent.parent.GetComponent<EquipmentController>().equipItem(assignedItem, assignedItem.holdingSpot, true);
            Destroy(gameObject);
        }
        else
        {
            bool isPlayerItem = transform.parent.name.Contains("Player");
            GameObject.Find("TransactionCanvas").GetComponent<TransactionController>().setCurrentlySelectedItem(this, isPlayerItem);
        }
    }

    public Item getAssignedItem()
    {
        return assignedItem;
    }
}
