﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;
    private static World world = null;

    public bool isGameLoaded;
    private List<Unit> missionUnits;
    private string currentBattleMap;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("All hero Units:");
            for (int i = 0; i < world.getHeroes().Count; i++)
            {
                Debug.Log(world.getHeroes()[i].getName() + "   " + world.getHeroes()[i].CurrentHealth + "   " + world.getHeroes()[i].MaxMovement);
            }
            Debug.Log("----------------------------------------------");
            Debug.Log("Missions :");
            for (int i = 0; i < world.getCurrentMissions().Count; i++)
            {
                Debug.Log(world.getCurrentMissions()[i].getMissionID() + "   "
                    + world.getCurrentMissions()[i].getMissionName() + "  Next Location: "
                    + world.getCurrentMissions()[i].getNextEventLocation() + " MissionScenes 0  "
                    + world.getCurrentMissions()[i].getMissionScenes().Length + "   "
                    + world.getCurrentMissions()[i].getMissionDescription());
            }
            Debug.Log("Missions Completed:");
            for (int i = 0; i < world.getCompletedMissions().Count; i++)
            {
                Debug.Log(world.getCompletedMissions()[i].getMissionID() + "   " + world.getCompletedMissions()[i].getMissionName() + "   " + world.getCompletedMissions()[i].getMissionDescription());
            }
        }
    }


    public void loadBattleGrounds(string sceneName, List<Unit> units)
    {
        missionUnits = units;
        currentBattleMap = sceneName;
        SceneManager.LoadScene("SampleScene"); //So far it loads generic battlegrounds map with different info depending on path
    }

    public List<Unit> getMissionUnits()
    {
        return missionUnits;
    }

    public string getCurrentBattleMap()
    {
        return currentBattleMap;
    }

    public World getWorld()
    {
        return world;
    }

    public void setWorld(World newWorld)
    {
        world = newWorld;
    }

    public void loadSavedInfo(string filePath)
    {
        XmlSerializer serialiazer = new XmlSerializer(typeof(World));
        TextReader reader = new StreamReader(filePath);
        world = (World)serialiazer.Deserialize(reader);
        reader.Close();

        SceneManager.LoadScene(world.getCurrentScene());
    }

    public void saveGame(string filePath)
    {
        if (world == null)
        {
            Debug.Log("Warning: World is null and save might not be created!");
        }

        XmlSerializer serialiazer = new XmlSerializer(typeof(World));
        TextWriter writer = new StreamWriter(filePath);
        serialiazer.Serialize(writer, world);
        writer.Close();
    }

}
