﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class DialogController : MonoBehaviour
{

    private Text dialogText;
    private string[][] worldDialogs;
    private Mission assignedMission;
    private RawImage portrait;
    private AudioSource audioSource;

    UnityEngine.Object _dialogChoiceButton;

    void Start()
    {
        dialogText = transform.GetChild(0).GetChild(0).GetComponentInChildren<Text>();

        _dialogChoiceButton = Resources.Load("Prefabs/DialogChoice");
        worldDialogs = FileReader.readMapDialogs(Application.dataPath + "/Maps/DialogWorld.txt");
        portrait = transform.GetChild(1).GetComponentInChildren<RawImage>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {

    }

    public void setAssignedMission(Mission assignedMission)
    {
        this.assignedMission = assignedMission;
    }

    private void createNewCurrentMission()
    {
        GameObject.Find("WorldController").GetComponent<WorldController>().createNewMission(assignedMission);
    }


    public void setDialogs(string id)
    {
        for (int i = 0; i < dialogText.transform.parent.childCount; i++)
        {
            if (dialogText.transform.parent.GetChild(i).GetComponent<Button>() != null)
            {
                Destroy(dialogText.transform.parent.GetChild(i).gameObject);
            }
        }
        if (id.Equals("TRANSACTION_WINDOW"))
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
            GameObject.Find("TransactionCanvas").GetComponent<TransactionController>().transactionWindowAction();
        }
        else if (id.Equals("CLOSE_WINDOW"))
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            string dialogLine = findDialog(id);
            formatDialogLine(dialogLine);
        }
    }

    private string findDialog(string id)
    {
        for (int i = 0; i < worldDialogs.Length; i++)
        {
            if (worldDialogs[i][0].Split('[', ']')[1].Equals(id))
            {
                return worldDialogs[i][1];
            }
        }
        return "ERROR: I should have not reach this statement... I should have not";
    }

    private void formatDialogLine(string dialogLine)
    {
        string[] lineArray = dialogLine.Split('*');
        for (int i = 0; i < lineArray.Length; i++)
        {
            if (checkForBattlegrounds(lineArray))
            {
                GameObject.Find("HeroSelectionCanvas").GetComponent<HeroSelectionController>().setHeroSelectionController(lineArray[1], int.Parse(lineArray[2]));
                transform.GetChild(0).gameObject.SetActive(false);
                break;
            }
            if (i == 0)
            {
                dialogText.text = formatSpecialChars(lineArray[i]);
            }
            if (i == 1)
            {
                if (lineArray[i].Equals("NONE_PORTRAIT"))
                {
                    portrait.transform.parent.gameObject.SetActive(false);
                }
                else
                {
                    portrait.transform.parent.gameObject.SetActive(true);
                    portrait.texture = Resources.Load<Texture2D>("Prefabs/Textures/" + lineArray[i]);
                }
            }
            if (i == 2)
            {
                audioSource.PlayOneShot(Resources.Load<AudioClip>("Prefabs/Sounds/" + lineArray[i]));
            }
            else
            {
                if (Char.IsLetter(lineArray[i][0]))
                {
                    additionalChoiceEffects(lineArray[i]);
                    continue;
                }
                string answer = findDialog(lineArray[i]);
                string[] answerLines = answer.Split('*');
                GameObject dialogButton = (GameObject)GameObject.Instantiate(_dialogChoiceButton);
                dialogButton.GetComponentInChildren<Text>().text = formatSpecialChars(answerLines[0]);
                dialogButton.GetComponent<DialogWorldChoice>().setId(answerLines[1]);
                dialogButton.transform.SetParent(transform.GetChild(0).GetChild(0));
            }
        }
    }

    private string formatSpecialChars(string line)
    {
        line = line.Replace("#", "\n\n");
        line = line.Replace("(", "[");
        line = line.Replace(")", "]");
        return line;
    }

    private void additionalChoiceEffects(string value)
    {
        if (value.Contains("GOLD"))
        {
            Debug.Log("GOLD");
        }
        if (value.Contains("REPUTATION"))
        {
            Debug.Log("REPUTATION");
        }
        if (value.Contains("START_MISSION"))
        {
            createNewCurrentMission();
        }
    }

    private bool checkForBattlegrounds(string[] line)
    {
        if (line[0].Equals("BATTLEGROUND"))
        {
            return true;
        }
        return false;
    }

}
